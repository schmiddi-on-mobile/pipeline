use adw::prelude::GtkApplicationExt;
use gdk::subclass::prelude::ObjectSubclassIsExt;
use gtk::glib::Object;
use gtk::prelude::GtkWindowExt;
use gtk::prelude::SettingsExt;

gtk::glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::Window>)
        @extends adw::ApplicationWindow, gtk::ApplicationWindow, adw::Window, gtk::Window, gtk::Widget,
        @implements gtk::gio::ActionGroup, gtk::gio::ActionMap, gtk::Accessible, gtk::Buildable,
            gtk::ConstraintTarget, gtk::Native, gtk::Root, gtk::ShortcutManager;
}

impl Window {
    pub fn new(app: &gtk::Application) -> Self {
        app.set_accels_for_action("win.settings", &["<Control>comma"]);
        app.set_accels_for_action("win.about", &["F1"]);
        app.set_accels_for_action("win.show-help-overlay", &["<Control>question"]);
        app.set_accels_for_action("win.go-back", &["<Alt>Left"]);
        app.set_accels_for_action("win.search", &["<Ctrl>f", "F3"]);
        app.set_accels_for_action("win.subscribe-or-watch-later", &["<Ctrl>d"]);
        app.set_accels_for_action("window.close", &["<Control>q", "<Control>w"]);
        app.set_accels_for_action("win.reload", &["<Control>r", "F5"]);

        app.set_accels_for_action("win.tab-feed", &["<Control>1"]);
        app.set_accels_for_action("win.tab-watch-later", &["<Control>2"]);
        app.set_accels_for_action("win.tab-subscriptions", &["<Control>3"]);

        Object::builder::<Self>()
            .property("application", app)
            .build()
    }

    fn save_window_size(&self) -> Result<(), gtk::glib::BoolError> {
        let imp = self.imp();

        let (width, height) = self.default_size();

        imp.settings.set_int("window-width", width)?;
        imp.settings.set_int("window-height", height)?;

        imp.settings
            .set_boolean("is-maximized", self.is_maximized())?;

        Ok(())
    }

    fn load_window_size(&self) {
        let imp = self.imp();

        let width = imp.settings.int("window-width");
        let height = imp.settings.int("window-height");
        let is_maximized = imp.settings.boolean("is-maximized");

        self.set_default_size(width, height);

        if is_maximized {
            self.maximize();
        }
    }

    pub fn display_error(&self, text: String, error: Box<dyn std::error::Error>) {
        // Ignore an error which happens whenever a YouTube video is opened and which therefore is very annoying.
        // See the upstream error: https://github.com/TeamPiped/Piped/issues/3658.
        if format!("{:?}", error).contains("Sign in to confirm that you're not a bot") {
            return;
        }

        let imp = self.imp();
        let mut errors = imp.errors.borrow_mut();
        errors.push((text.clone(), error));

        let num_errors = errors.len();

        let toast_title = if num_errors == 1 {
            text
        } else {
            gettextrs::ngettext("One Error", "{} Errors", num_errors as u32)
                .replace("{}", &num_errors.to_string())
        };

        imp.toast_error.set_title(&toast_title);

        imp.toast_overlay.add_toast(imp.toast_error.get());
    }
}

pub mod imp {
    use std::cell::{Cell, RefCell};
    use std::time::Duration;

    use crate::backend::{
        Channel, Client, PlayType, SearchStream, SubscriptionsStore, Video, VideoFetcherStream,
    };
    use crate::config::{self, APP_ID, PROFILE};
    use crate::gspawn;
    use crate::gui::channel_item::ChannelItem;
    use crate::gui::channel_page::ChannelPage;
    use crate::gui::error_dialog::ErrorDialog;
    use crate::gui::filter_dialog::FilterDialog;
    use crate::gui::import_dialog::ImportDialog;
    use crate::gui::preferences_dialog::PreferencesDialog;
    use crate::gui::search_list::SearchList;
    use crate::gui::subscriptions_list::SubscriptionsList;
    use crate::gui::utility::Utility;
    use crate::gui::video_item::VideoItem;
    use crate::gui::video_list::VideoList;
    use crate::gui::video_page::VideoPage;
    use crate::gui::watch_later_list::WatchLaterList;

    use adw::prelude::AdwDialogExt;
    use adw::{NavigationSplitView, Toast, ToastOverlay, ViewStack, ViewStackPage, WindowTitle};
    use gdk::gio::{SimpleAction, SimpleActionGroup};
    use glib::subclass::InitializingObject;
    use glib::{clone, Properties, Variant};
    use gtk::glib::Propagation;
    use gtk::subclass::prelude::*;
    use gtk::{glib, Builder, SearchEntry, ShortcutsWindow};
    use gtk::{prelude::*, SearchBar};

    use adw::subclass::prelude::AdwApplicationWindowImpl;
    use adw::subclass::prelude::AdwWindowImpl;
    use gtk::CompositeTemplate;

    #[derive(CompositeTemplate, Properties)]
    #[properties(wrapper_type = super::Window)]
    #[template(resource = "/ui/window.ui")]
    pub struct Window {
        #[template_child]
        main_stack: TemplateChild<ViewStack>,
        #[template_child]
        sidebar_stack: TemplateChild<ViewStack>,

        #[template_child]
        main_title: TemplateChild<WindowTitle>,
        #[template_child]
        sidebar_title: TemplateChild<WindowTitle>,

        #[template_child]
        split_view: TemplateChild<NavigationSplitView>,
        #[template_child]
        video_list: TemplateChild<VideoList>,

        #[template_child]
        video_page: TemplateChild<VideoPage>,
        #[template_child]
        channel_page: TemplateChild<ChannelPage>,

        #[template_child]
        search_entry: TemplateChild<SearchEntry>,
        #[template_child]
        search_bar: TemplateChild<SearchBar>,
        #[template_child]
        search_list: TemplateChild<SearchList>,
        #[template_child]
        search_page: TemplateChild<ViewStackPage>,

        #[template_child]
        pub(super) toast_overlay: TemplateChild<ToastOverlay>,
        #[template_child]
        pub(super) toast_error: TemplateChild<Toast>,

        pub(super) errors: RefCell<Vec<(String, Box<dyn std::error::Error>)>>,

        #[property(get)]
        client: RefCell<Client>,

        #[property(get, set)]
        breakpoint_collapsed: Cell<bool>,

        pub settings: gtk::gio::Settings,
    }

    impl Default for Window {
        fn default() -> Self {
            Self {
                main_stack: Default::default(),
                sidebar_stack: Default::default(),
                main_title: Default::default(),
                sidebar_title: Default::default(),
                split_view: Default::default(),
                video_list: Default::default(),
                video_page: Default::default(),
                channel_page: Default::default(),
                search_page: Default::default(),
                search_entry: Default::default(),
                search_bar: Default::default(),
                search_list: Default::default(),
                toast_overlay: Default::default(),
                toast_error: Default::default(),

                errors: Default::default(),

                settings: gtk::gio::Settings::new(APP_ID),

                breakpoint_collapsed: Default::default(),
                client: Default::default(),
            }
        }
    }

    #[gtk::template_callbacks]
    impl Window {
        #[template_callback(function)]
        fn reload_button_visible(page: Option<&str>) -> bool {
            page == Some("feed")
        }

        #[template_callback(function)]
        fn is_video_visible(page: Option<&str>) -> bool {
            page == Some("video-page")
        }

        #[template_callback(function)]
        fn is_channel_visible(page: Option<&str>) -> bool {
            page == Some("channel-page")
        }

        #[template_callback(function)]
        fn refresh_button_visible_child_name(loading: bool) -> &'static str {
            if loading {
                "spinner"
            } else {
                "button"
            }
        }

        #[template_callback]
        fn split_view_show_content(&self) {
            if !self.split_view.shows_content() {
                self.video_page.pause();
                gspawn!(clone!(
                    #[strong(rename_to=video_page)]
                    self.video_page,
                    async move {
                        // Add timeout, otherwise segfault.
                        glib::timeout_future(Duration::from_millis(100)).await;
                        video_page.set_fullscreened(false);
                    }
                ));
            }
        }

        #[template_callback]
        fn go_back(&self) {
            let has_video = self.video_page.video().is_some();
            let has_channel = self.channel_page.channel().is_some();
            let currently_displaying_video =
                self.main_stack.visible_child_name() == Some("video-page".into());
            let currently_displaying_channel =
                self.main_stack.visible_child_name() == Some("channel-page".into());

            if !(has_video && has_channel) {
                // Only one thing displayed, going back closes the main dialog.
                self.video_page.set_video(None::<Video>);
                self.video_page.pause();
                self.channel_page.set_channel(None::<Channel>);
                self.split_view.set_show_content(false);
            } else if currently_displaying_channel {
                // Went from a video to a channel, go back to only displaying the video.
                self.channel_page.set_channel(None::<Channel>);
                self.main_stack.set_visible_child_name("video-page");
            } else if currently_displaying_video {
                // Went from a channel to a video, go back to only displaying the channel.
                self.video_page.set_video(None::<Video>);
                self.video_page.pause();
                self.main_stack.set_visible_child_name("channel-page");
            } else {
                log::warn!("Not sure how to go back; closing the main dialog");
                self.video_page.set_video(None::<Video>);
                self.video_page.pause();
                self.channel_page.set_channel(None::<Channel>);
                self.split_view.set_show_content(false);
            }
        }

        #[template_callback]
        fn play_video(&self, video: Video, play_type: PlayType) {
            self.channel_page.set_channel(None::<Channel>);
            self.play_video_from_channel(video, play_type);
        }

        #[template_callback]
        fn play_video_from_channel(&self, video: Video, play_type: PlayType) {
            self.video_page.set_video(Some(video));
            self.split_view.set_show_content(true);
            self.main_stack.set_visible_child_name("video-page");

            self.video_page.grab_focus();

            let play_is_default = play_type == PlayType::Default;
            let play_external_default = self.settings.boolean("play-external-default");

            if play_is_default ^ play_external_default {
                self.video_page.play();
            } else {
                self.video_page.play_external();
            }
        }

        #[template_callback]
        fn display_channel(&self, channel: Channel) {
            self.channel_page.set_channel(Some(channel));
            self.video_page.pause();
            self.video_page.set_video(None::<Video>);
            self.split_view.set_show_content(true);
            self.main_stack.set_visible_child_name("channel-page");
        }

        #[template_callback]
        fn display_channel_from_video(&self, channel: Channel) {
            self.channel_page.set_channel(Some(channel));
            self.video_page.pause();
            self.split_view.set_show_content(true);
            self.main_stack.set_visible_child_name("channel-page");
        }

        #[template_callback]
        fn search(&self) {
            gspawn!(clone!(
                #[strong(rename_to = obj)]
                self.obj(),
                async move {
                    let s = obj.imp();
                    let text = s.search_entry.text();
                    s.sidebar_stack.set_visible_child_name("search");
                    s.search_list.set_model(obj.client().search(&text).await);
                }
            ));
        }

        #[template_callback]
        fn sidebar_child_changed(&self) {
            let is_search_page = self.sidebar_stack.visible_child_name() == Some("search".into());
            let is_feed_page = self.sidebar_stack.visible_child_name() == Some("feed".into());
            self.search_page.set_visible(is_search_page);
            self.search_bar.set_search_mode(is_search_page);

            self.sidebar_title.set_title(
                &self
                    .sidebar_stack
                    .page(&self.sidebar_stack.visible_child().unwrap())
                    .title()
                    .unwrap(),
            );

            let obj = self.obj();
            obj.action_set_enabled("win.filter-enabled", is_feed_page);
            obj.action_set_enabled("win.filter-dialog", is_feed_page);
        }

        #[template_callback]
        fn main_child_changed(&self) {
            self.main_title.set_title(
                &self
                    .main_stack
                    .page(&self.main_stack.visible_child().unwrap())
                    .title()
                    .unwrap(),
            );
        }

        #[template_callback]
        fn copy_video_url_from_video_page(&self) {
            if let Some(video) = self.video_page.video() {
                self.copy_video_url(video);
            }
        }

        #[template_callback]
        fn copy_video_url(&self, video: Video) {
            let clipboard = WidgetExt::display(&*self.obj()).clipboard();
            clipboard.set_text(video.playable_url().as_ref());

            let toast = Toast::builder()
                .title(gettextrs::gettext("Copied URL"))
                .build();
            self.toast_overlay.add_toast(toast);
        }

        #[template_callback]
        fn download_from_video_page(&self) {
            if let Some(video) = self.video_page.video() {
                self.download(video);
            }
        }

        #[template_callback]
        fn download(&self, video: Video) {
            // TODO: Possibly have some UI for that, instead of just displaying toasts.
            let obj = self.obj();
            let url = video.playable_url();
            let downloader = self.settings.string("downloader");

            gspawn!(clone!(
                #[strong]
                obj,
                async move {
                    let s = obj.imp();

                    let toast = Toast::builder()
                        .title(gettextrs::gettext("Started Download"))
                        .build();
                    s.toast_overlay.add_toast(toast);

                    let res = Utility::run_command_on_url(downloader, url).await;
                    if let Err(e) = res {
                        obj.display_error(
                            gettextrs::gettext("Failed to spawn video downloader"),
                            e,
                        );
                    } else {
                        let toast = Toast::builder()
                            .title(gettextrs::gettext("Finished Download"))
                            .build();
                        s.toast_overlay.add_toast(toast);
                    }
                }
            ));
        }

        fn setup_actions(&self, obj: &super::Window) {
            let action_settings = SimpleAction::new("settings", None);
            action_settings.connect_activate(clone!(
                #[weak]
                obj,
                move |_, _| {
                    let settings = PreferencesDialog::new();
                    settings.present(Some(&obj));
                }
            ));

            let action_toggle_filter = self.settings.create_action("filter-enabled");

            let action_filter_dialog = SimpleAction::new("filter-dialog", None);
            action_filter_dialog.connect_activate(clone!(
                #[weak]
                obj,
                move |_, _| {
                    let filter_dialog = FilterDialog::new(obj.client());
                    filter_dialog.present(Some(&obj));
                }
            ));

            let action_about = SimpleAction::new("about", None);
            action_about.connect_activate(clone!(
                #[weak]
                obj,
                move |_, _| {
                    let about_dialog = adw::AboutDialog::from_appdata(
                        &(config::RESOURCES_PATH.to_owned() + config::APP_ID + ".metainfo.xml"),
                        Some(env!("CARGO_PKG_VERSION")),
                    );

                    about_dialog.set_comments(env!("CARGO_PKG_DESCRIPTION"));

                    about_dialog.set_developers(
                        &(env!("CARGO_PKG_AUTHORS").split(':').collect::<Vec<&str>>()),
                    );
                    about_dialog.set_artists(&["David Lapshin <ddaudix@gmail.com>"]);
                    about_dialog.present(Some(&obj));
                }
            ));

            let action_show_errors = SimpleAction::new("show-errors", None);
            action_show_errors.connect_activate(clone!(
                #[weak]
                obj,
                move |_, _| {
                    let dialog = ErrorDialog::new(&obj.imp().errors.borrow());
                    dialog.present(Some(&obj));
                }
            ));
            let action_clear_errors = SimpleAction::new("clear-errors", None);
            action_clear_errors.connect_activate(clone!(
                #[weak]
                obj,
                move |_, _| {
                    obj.imp().errors.borrow_mut().clear();
                }
            ));

            let action_show_help_overlay = SimpleAction::new("show-help-overlay", None);
            action_show_help_overlay.connect_activate(clone!(
                #[weak]
                obj,
                move |_, _| {
                    let builder = Builder::from_resource("/ui/shortcuts.ui");
                    let shortcuts_window: ShortcutsWindow = builder
                        .object("help_overlay")
                        .expect("shortcuts.ui to have at least one object help_overlay");
                    shortcuts_window.set_transient_for(Some(&obj));
                    shortcuts_window.present();
                }
            ));

            let action_watch_later = SimpleAction::new("watch-later", None);
            action_watch_later.connect_activate(clone!(
                #[weak]
                obj,
                move |_, _| {
                    if let Some(video) = obj.imp().video_page.video() {
                        obj.client().toggle_watch_later(video);
                    }
                }
            ));
            let action_subscribe = SimpleAction::new("subscribe", None);
            action_subscribe.connect_activate(clone!(
                #[weak]
                obj,
                move |_, _| {
                    if let Some(channel) = obj.imp().channel_page.channel() {
                        obj.client().toggle_subscription(channel);
                    }
                }
            ));
            let action_subscribe_or_watch_later =
                SimpleAction::new("subscribe-or-watch-later", None);
            action_subscribe_or_watch_later.connect_activate(clone!(
                #[weak]
                obj,
                move |_, _| {
                    let s = obj.imp();
                    let current_page = s.main_stack.visible_child_name();
                    if self::Window::is_channel_visible(current_page.as_deref()) {
                        let _ = WidgetExt::activate_action(&obj, "win.subscribe", None::<&Variant>);
                    } else if self::Window::is_video_visible(current_page.as_deref()) {
                        let _ =
                            WidgetExt::activate_action(&obj, "win.watch-later", None::<&Variant>);
                    }
                }
            ));

            let action_go_back = SimpleAction::new("go-back", None);
            action_go_back.connect_activate(clone!(
                #[weak]
                obj,
                move |_, _| {
                    obj.imp().go_back();
                }
            ));
            let action_search = SimpleAction::new("search", None);
            action_search.connect_activate(clone!(
                #[weak]
                obj,
                move |_, _| {
                    obj.imp().search_bar.set_search_mode(true);
                }
            ));
            let action_reload = SimpleAction::new("reload", None);
            action_reload.connect_activate(clone!(
                #[weak]
                obj,
                move |_, _| {
                    let s = obj.imp();
                    let current_page = s.main_stack.visible_child_name();
                    if self::Window::is_channel_visible(current_page.as_deref()) {
                        let _ = WidgetExt::activate_action(
                            &obj,
                            "channel-page.reload",
                            None::<&Variant>,
                        );
                    } else {
                        let _ = WidgetExt::activate_action(&obj, "feed.reload", None::<&Variant>);
                    }
                }
            ));

            let action_tab_feed = SimpleAction::new("tab-feed", None);
            action_tab_feed.connect_activate(clone!(
                #[weak]
                obj,
                move |_, _| {
                    obj.imp().sidebar_stack.set_visible_child_name("feed");
                }
            ));
            let action_tab_watch_later = SimpleAction::new("tab-watch-later", None);
            action_tab_watch_later.connect_activate(clone!(
                #[weak]
                obj,
                move |_, _| {
                    obj.imp()
                        .sidebar_stack
                        .set_visible_child_name("watch-later");
                }
            ));
            let action_tab_subscriptions = SimpleAction::new("tab-subscriptions", None);
            action_tab_subscriptions.connect_activate(clone!(
                #[weak]
                obj,
                move |_, _| {
                    obj.imp()
                        .sidebar_stack
                        .set_visible_child_name("subscriptions");
                }
            ));

            let action_import = SimpleAction::new("import", None);
            action_import.connect_activate(clone!(
                #[weak]
                obj,
                move |_, _| {
                    let dialog = ImportDialog::new(obj.client());
                    dialog.present(Some(&obj));
                }
            ));

            let actions = SimpleActionGroup::new();
            obj.insert_action_group("win", Some(&actions));
            actions.add_action(&action_settings);
            actions.add_action(&action_toggle_filter);
            actions.add_action(&action_filter_dialog);
            actions.add_action(&action_show_help_overlay);
            actions.add_action(&action_about);
            actions.add_action(&action_show_errors);
            actions.add_action(&action_clear_errors);
            actions.add_action(&action_watch_later);
            actions.add_action(&action_subscribe);
            actions.add_action(&action_subscribe_or_watch_later);
            actions.add_action(&action_go_back);
            actions.add_action(&action_search);
            actions.add_action(&action_reload);
            actions.add_action(&action_tab_feed);
            actions.add_action(&action_tab_watch_later);
            actions.add_action(&action_tab_subscriptions);
            actions.add_action(&action_import);

            let action_feed_reload = SimpleAction::new("reload", None);
            action_feed_reload.connect_activate(clone!(
                #[weak]
                obj,
                move |_, _| {
                    obj.imp().video_list.reload();
                }
            ));

            let action_channel_page_reload = SimpleAction::new("reload", None);
            action_channel_page_reload.connect_activate(clone!(
                #[weak]
                obj,
                move |_, _| {
                    obj.imp().channel_page.reload();
                }
            ));

            let actions_feed = SimpleActionGroup::new();
            obj.insert_action_group("feed", Some(&actions_feed));
            actions_feed.add_action(&action_feed_reload);

            let actions_channel_page = SimpleActionGroup::new();
            obj.insert_action_group("channel-page", Some(&actions_channel_page));
            actions_channel_page.add_action(&action_channel_page_reload);
        }

        fn setup_client(&self) {
            let obj = self.obj();
            let client = obj.client();

            let res = client.setup();
            if let Err(e) = res {
                log::error!("Error setting up the client: {}", e);
                obj.display_error(gettextrs::gettext("Failed to set up application"), e);
            }

            if self.settings.boolean("reload-on-startup") {
                self.video_list.reload();
            }
        }

        fn setup_search(&self) {
            self.search_bar.connect_entry(&self.search_entry.get());
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Window {
        const NAME: &'static str = "TFWindow";
        type Type = super::Window;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            VideoItem::ensure_type();
            ChannelItem::ensure_type();
            VideoList::ensure_type();
            WatchLaterList::ensure_type();
            SubscriptionsList::ensure_type();
            VideoFetcherStream::ensure_type();
            SubscriptionsStore::ensure_type();
            SearchList::ensure_type();
            SearchStream::ensure_type();
            Self::bind_template(klass);
            Self::bind_template_callbacks(klass);
            Utility::bind_template_callbacks(klass);
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for Window {
        fn constructed(&self) {
            self.parent_constructed();
            self.setup_search();
            self.setup_actions(&self.obj());

            self.setup_client();
            self.main_child_changed();
            self.sidebar_child_changed();

            let obj = self.obj();
            if PROFILE == "Devel" {
                obj.add_css_class("devel");
            }
            obj.load_window_size();
        }
    }

    impl WidgetImpl for Window {}
    impl WindowImpl for Window {
        fn close_request(&self) -> Propagation {
            let obj = self.obj();
            if let Err(err) = obj.save_window_size() {
                log::warn!("Failed to save window state, {}", &err);
            }

            let _ = obj.client().cleanup();

            self.parent_close_request()
        }
    }
    impl ApplicationWindowImpl for Window {}
    impl AdwWindowImpl for Window {}
    impl AdwApplicationWindowImpl for Window {}
}
