use glib::{clone, object::Cast};
use gtk::prelude::WidgetExt;

use crate::gspawn;

use super::window::Window;

gtk::glib::wrapper! {
    pub struct ChannelPage(ObjectSubclass<imp::ChannelPage>)
        @extends gtk::Box, gtk::Widget,
        @implements gtk::gio::ActionGroup, gtk::gio::ActionMap, gtk::Accessible, gtk::Buildable,
            gtk::ConstraintTarget;
}

const LOAD_COUNT: usize = 10;

impl ChannelPage {
    pub fn reload(&self) {
        if let Some(channel) = self.channel() {
            let client = self.client();
            // Load additional data
            gspawn!(clone!(
                #[strong]
                client,
                #[strong]
                channel,
                #[strong(rename_to = s)]
                self,
                async move {
                    let result = client.populate_channel_information(&channel.id()).await;
                    if let Err(e) = result {
                        log::error!("Failed to populate channel information: {}", e);
                        s.window().display_error(
                            gettextrs::gettext("Failed to load channel data"),
                            Box::new(e),
                        );
                    }

                    // Load videos
                    let videos = client.videos_for_channels(&[channel.id()]).await;
                    videos.load_more(LOAD_COUNT);
                    s.set_model(videos);
                }
            ));
        }
    }

    fn window(&self) -> Window {
        self.root().unwrap().dynamic_cast().unwrap()
    }
}

pub mod imp {
    use std::cell::RefCell;

    use gdk::gio::SimpleAction;
    use gdk::gio::SimpleActionGroup;
    use glib::clone;
    use glib::closure;
    use glib::closure_local;
    use glib::subclass::InitializingObject;
    use glib::subclass::Signal;
    use glib::BoxedAnyObject;
    use glib::Object;
    use glib::Properties;
    use gtk::glib;
    use gtk::prelude::*;
    use gtk::subclass::prelude::*;
    use gtk::Button;
    use gtk::CompositeTemplate;
    use gtk::GridView;
    use gtk::PositionType;
    use gtk::SignalListItemFactory;
    use gtk::Widget;
    use once_cell::sync::Lazy;

    use crate::backend::Channel;
    use crate::backend::Client;
    use crate::backend::PlayType;
    use crate::backend::Video;
    use crate::backend::VideoFetcherStream;
    use crate::gui::channel_page::LOAD_COUNT;
    use crate::gui::utility::Utility;
    use crate::gui::video_item::VideoItem;

    #[derive(CompositeTemplate, Default, Properties)]
    #[template(resource = "/ui/channel_page.ui")]
    #[properties(wrapper_type = super::ChannelPage)]
    pub struct ChannelPage {
        #[template_child]
        video_list: TemplateChild<GridView>,
        #[template_child]
        button_subscribe: TemplateChild<Button>,

        #[property(get, set = Self::set_channel, nullable)]
        channel: RefCell<Option<Channel>>,

        #[property(get, set = Self::set_model)]
        model: RefCell<Option<VideoFetcherStream>>,

        #[property(get, set)]
        client: RefCell<Client>,
    }

    impl ChannelPage {
        fn set_model(&self, model: VideoFetcherStream) {
            let obj = self.obj();

            model.connect_local(
                "error",
                false,
                clone!(
                    #[weak]
                    obj,
                    #[upgrade_or_default]
                    move |err| {
                        let err = err[1]
                            .get::<BoxedAnyObject>()
                            .expect("Error to be BoxedAnyObject");
                        let err: &(String, RefCell<Option<pcore::Error>>) = &err.borrow();
                        obj.window().display_error(
                            err.0.clone(),
                            Box::new(err.1.take().expect("Error to be present")),
                        );
                        None
                    }
                ),
            );

            self.model.borrow_mut().replace(model);
        }

        // Recommended syntax is ugly and the item will be moved to `pedantic` soon.
        #[allow(clippy::assigning_clones)]
        fn set_channel(&self, channel: Option<Channel>) {
            *self.channel.borrow_mut() = channel.clone();
            self.obj().reload();
        }

        fn setup(&self) {
            let obj = self.obj();

            let factory = SignalListItemFactory::new();
            factory.connect_setup(clone!(
                #[strong]
                obj,
                move |_, list_item| {
                    let list_item = list_item.downcast_ref::<gtk::ListItem>().unwrap();
                    let video_item = VideoItem::new();
                    list_item.set_child(Some(&video_item));

                    list_item
                        .property_expression("item")
                        .bind(&video_item, "video", Widget::NONE);
                    obj.property_expression("client")
                        .bind(&video_item, "client", Widget::NONE);
                    video_item
                        .property_expression("video")
                        .chain_closure::<Option<String>>(closure!(
                            |_: Option<Object>, video: Option<Video>| {
                                video.map(Utility::accessible_description_for_video)
                            }
                        ))
                        .bind(list_item, "accessible-label", Widget::NONE);
                    video_item.connect_closure(
                        "play-video",
                        false,
                        closure_local!(
                            #[strong]
                            obj,
                            move |_: VideoItem, video: Video, play_type: PlayType| {
                                obj.emit_by_name::<()>("play-video", &[&video, &play_type]);
                            }
                        ),
                    );
                    video_item.connect_closure(
                        "copy-video-url",
                        false,
                        closure_local!(
                            #[strong]
                            obj,
                            move |_: VideoItem, video: Video| {
                                obj.emit_by_name::<()>("copy-video-url", &[&video]);
                            }
                        ),
                    );
                    video_item.connect_closure(
                        "download",
                        false,
                        closure_local!(
                            #[strong]
                            obj,
                            move |_: VideoItem, video: Video| {
                                obj.emit_by_name::<()>("download", &[&video]);
                            }
                        ),
                    );
                }
            ));
            self.video_list.set_factory(Some(&factory));

            self.video_list.set_single_click_activate(true);
            self.video_list.connect_activate(clone!(
                #[strong]
                obj,
                move |list_view, position| {
                    let model = list_view.model().expect("The model has to exist.");
                    let video = model
                        .item(position)
                        .expect("The item has to exist.")
                        .downcast::<Video>()
                        .expect("The item has to be an `Video`.");

                    obj.emit_by_name::<()>("play-video", &[&video, &PlayType::Default]);
                }
            ));
        }

        fn add_actions(&self) {
            let obj = self.obj();
            let action_more = SimpleAction::new("more-videos", None);
            action_more.connect_activate(clone!(
                #[strong]
                obj,
                move |_, _| {
                    if let Some(model) = obj.model() {
                        model.load_more(LOAD_COUNT);
                    }
                }
            ));
            let action_reload = SimpleAction::new("reload", None);
            action_reload.connect_activate(clone!(
                #[strong]
                obj,
                move |_, _| {
                    obj.reload();
                }
            ));

            let actions = SimpleActionGroup::new();
            obj.insert_action_group("channel-page", Some(&actions));
            actions.add_action(&action_more);
            actions.add_action(&action_reload);
        }
    }

    #[gtk::template_callbacks]
    impl ChannelPage {
        #[template_callback(function)]
        fn format_subscribers(subscribers: u64) -> String {
            gettextrs::ngettext(
                "one subscriber",
                "{} subscribers",
                subscribers.try_into().unwrap_or_default(),
            )
            .replace("{}", &Utility::format_number_compact(subscribers))
        }

        #[template_callback]
        fn subscribe_label(&self, subscribed: bool) -> String {
            if subscribed {
                self.button_subscribe.remove_css_class("suggested-action");
                gettextrs::gettext("Unsubscribe")
            } else {
                self.button_subscribe.add_css_class("suggested-action");
                gettextrs::gettext("Subscribe")
            }
        }

        #[template_callback]
        fn edge_reached(&self, pos: PositionType) {
            if pos == PositionType::Bottom {
                let _ = gtk::prelude::WidgetExt::activate_action(
                    self.obj().as_ref(),
                    "channel-page.more-videos",
                    None,
                );
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ChannelPage {
        const NAME: &'static str = "TFChannelPage";
        type Type = super::ChannelPage;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            Self::bind_template_callbacks(klass);
            Utility::bind_template_callbacks(klass);
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for ChannelPage {
        fn constructed(&self) {
            self.parent_constructed();
            self.setup();
            self.add_actions();
        }

        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
                vec![
                    Signal::builder("play-video")
                        .param_types([Video::static_type(), PlayType::static_type()])
                        .build(),
                    Signal::builder("copy-video-url")
                        .param_types([Video::static_type()])
                        .build(),
                    Signal::builder("download")
                        .param_types([Video::static_type()])
                        .build(),
                ]
            });
            SIGNALS.as_ref()
        }
    }

    impl WidgetImpl for ChannelPage {}
    impl BoxImpl for ChannelPage {}
}
