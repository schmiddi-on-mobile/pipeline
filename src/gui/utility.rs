use std::process::Stdio;

use chrono::{Datelike, Days, Local, TimeDelta, Utc};
use glib::Object;
use tokio::process::Command;
use url::Url;

use crate::{backend::Video, tspawn};

pub struct Utility {}

#[gtk::template_callbacks(functions)]
impl Utility {
    #[template_callback]
    fn or(#[rest] values: &[gtk::glib::Value]) -> bool {
        values
            .iter()
            .any(|v| v.get::<bool>().expect("Expected boolean for argument"))
    }

    #[template_callback]
    fn and(#[rest] values: &[gtk::glib::Value]) -> bool {
        values
            .iter()
            .all(|v| v.get::<bool>().expect("Expected boolean for argument"))
    }

    #[template_callback]
    fn not(#[rest] values: &[gtk::glib::Value]) -> bool {
        !values[0]
            .get::<bool>()
            .expect("Expected boolean for argument")
    }

    #[template_callback]
    fn is_none(value: Option<Object>) -> bool {
        value.is_none()
    }

    #[template_callback]
    fn is_some(value: Option<Object>) -> bool {
        value.is_some()
    }

    #[template_callback]
    fn escape_description(description: Option<String>) -> Option<String> {
        // YouTube allows some tags in the description, which are not supported by GTK by default.
        description.map(|d| {
            d.replace("<br>", "\n")
                .replace("&nbsp;", "\u{00A0}")
                .replace('&', "&amp;")
        })
    }

    #[template_callback]
    pub fn format_number_compact(number: u64) -> String {
        if number > 1_000_000_000 {
            // Note to translators: Format numbers compactly. For numbers at least one billion.
            gettextrs::pgettext("number-formatting", "{}B")
                .replace("{}", &format!("{:.2}", (number as f64) / 1e9))
        } else if number > 1_000_000 {
            // Note to translators: Format numbers compactly. For numbers at least one million.
            gettextrs::pgettext("number-formatting", "{}M")
                .replace("{}", &format!("{:.2}", (number as f64) / 1e6))
        } else if number > 1_000 {
            // Note to translators: Format numbers compactly. For numbers at least one thousand.
            gettextrs::pgettext("number-formatting", "{}K")
                .replace("{}", &format!("{:.2}", (number as f64) / 1e3))
        } else {
            number.to_string()
        }
    }

    #[template_callback]
    fn pixbuf_to_paintable(pixbuf: Option<gtk::gdk_pixbuf::Pixbuf>) -> Option<gdk::Texture> {
        pixbuf.as_ref().map(gdk::Texture::for_pixbuf)
    }

    #[template_callback]
    fn resource_for_platform(platform: Option<String>) -> Option<String> {
        platform.map(|p| format!("/de/schmidhuberj/tubefeeder/icons/platforms/{}.svg", p))
    }

    pub fn accessible_description_for_video(video: Video) -> String {
        gettextrs::gettext("{title} uploaded by {channel} on {date}")
            .replace("{title}", &video.title())
            .replace("{channel}", &video.uploader().name())
            .replace("{date}", &video.uploaded_label())
    }

    pub fn format_date_human(time: &chrono::DateTime<Utc>) -> String {
        let time = time.with_timezone(&Local);
        let date = time.date_naive();
        let today = Utc::now().date_naive();

        if today == date {
            // Translators: formatting of time of day in a human-readable fashion, see https://docs.rs/chrono/latest/chrono/format/strftime/index.html#specifiers
            time.format(&gettextrs::gettext("%H:%M")).to_string()
        } else if today + Days::new(1) == date {
            gettextrs::gettext("Tomorrow")
        } else if today - Days::new(1) == date {
            gettextrs::gettext("Yesterday")
        } else if today.year() == date.year() {
            // Translators: formatting of dates without year in a human-readable fashion, see https://docs.rs/chrono/latest/chrono/format/strftime/index.html#specifiers
            date.format(&gettextrs::gettext("%a, %d. %B")).to_string()
        } else {
            // Translators: formatting of dates with year in a human-readable fashion, see https://docs.rs/chrono/latest/chrono/format/strftime/index.html#specifiers
            date.format(&gettextrs::gettext("%Y-%m-%d")).to_string()
        }
    }

    pub fn format_duration_human(d: TimeDelta) -> String {
        if d.num_hours() > 0 {
            format!(
                "{}:{:0>2}:{:0>2}",
                d.num_hours(),
                (d - TimeDelta::try_hours(d.num_hours()).unwrap_or_default()).num_minutes(),
                (d - TimeDelta::try_minutes(d.num_minutes()).unwrap_or_default()).num_seconds()
            )
        } else {
            format!(
                "{:0>2}:{:0>2}",
                d.num_minutes(),
                (d - TimeDelta::try_minutes(d.num_minutes()).unwrap_or_default()).num_seconds()
            )
        }
    }

    pub async fn run_command_on_url<S: AsRef<str>>(
        command: S,
        url: Url,
    ) -> Result<(), Box<dyn std::error::Error + Sync + Send>> {
        let mut command = command.as_ref().to_string();
        if crate::config::FLATPAK {
            command = format!("flatpak-spawn --host {}", command);
        }

        tspawn!(async move {
            let mut player_iter = command.split(' ');
            let program = player_iter.next().expect("Non-empty player to be defined");
            let args: Vec<_> = player_iter.collect();

            let stdout = if log::log_enabled!(log::Level::Debug) {
                Stdio::inherit()
            } else {
                Stdio::null()
            };

            let stderr = if log::log_enabled!(log::Level::Error) {
                Stdio::inherit()
            } else {
                Stdio::null()
            };

            let child = Command::new(program)
                .args(args)
                .arg(url.to_string())
                .stdout(stdout)
                .stderr(stderr)
                .stdin(Stdio::null())
                .spawn();

            match child {
                Ok(mut child) => {
                    log::debug!("Successfully spawned child program");
                    match child.wait().await {
                        Ok(s) => {
                            if !s.success() {
                                Err(Box::<dyn std::error::Error + Sync + Send>::from(format!(
                                    "The external player did not terminate successfully, code: {}",
                                    s.code().unwrap_or(-1)
                                )))
                            } else {
                                Ok(())
                            }
                        }
                        Err(e) => Err(Box::new(e) as Box<dyn std::error::Error + Sync + Send>),
                    }
                }
                Err(e) => {
                    log::error!("Failed to spawn child program");
                    Err(Box::new(e) as Box<dyn std::error::Error + Sync + Send>)
                }
            }
        })
        .await
        .expect("Failed to spawn tokio")
    }
}
