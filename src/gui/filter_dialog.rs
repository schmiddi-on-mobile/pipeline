use gdk::glib::Object;
use glib::subclass::types::ObjectSubclassIsExt;

use crate::backend::Client;

gtk::glib::wrapper! {
    pub struct FilterDialog(ObjectSubclass<imp::FilterDialog>)
        @extends adw::PreferencesDialog, adw::Dialog, gtk::Widget,
        @implements gtk::gio::ActionGroup, gtk::gio::ActionMap, gtk::Accessible, gtk::Buildable,
            gtk::ConstraintTarget, gtk::Native, gtk::Root, gtk::ShortcutManager;
}

impl FilterDialog {
    pub fn new(client: Client) -> Self {
        let s: Self = Object::builder().property("client", client).build();
        // Requires client to be set up, otherwise the filter list would be empty in `constructed`.
        s.imp().init_custom_filters();
        s
    }
}

pub mod imp {
    use std::cell::RefCell;

    use adw::prelude::ActionRowExt;
    use adw::subclass::prelude::AdwDialogImpl;
    use adw::subclass::prelude::PreferencesDialogImpl;
    use adw::ActionRow;
    use adw::EntryRow;
    use adw::SwitchRow;
    use gdk::gio::Settings;
    use gdk::gio::SettingsBindFlags;
    use gdk::prelude::ListModelExt;
    use gdk::prelude::SettingsExtManual;
    use glib::clone;
    use glib::object::Cast;
    use glib::subclass::InitializingObject;
    use glib::Properties;
    use gtk::glib;
    use gtk::prelude::ButtonExt;
    use gtk::prelude::EditableExt;
    use gtk::prelude::ObjectExt;
    use gtk::prelude::WidgetExt;
    use gtk::subclass::prelude::*;
    use gtk::Align;
    use gtk::Button;
    use gtk::CompositeTemplate;
    use gtk::Label;
    use gtk::ListBox;

    use crate::backend::Client;
    use crate::backend::Filter;

    #[derive(CompositeTemplate, Properties)]
    #[properties(wrapper_type = super::FilterDialog)]
    #[template(resource = "/ui/filter_dialog.ui")]
    pub struct FilterDialog {
        #[template_child]
        label_filters_disabled: TemplateChild<Label>,

        #[template_child]
        switch_hide_shorts: TemplateChild<SwitchRow>,
        #[template_child]
        switch_hide_today: TemplateChild<SwitchRow>,

        #[template_child]
        entry_title: TemplateChild<EntryRow>,
        #[template_child]
        entry_channel: TemplateChild<EntryRow>,

        #[template_child]
        custom_filter_box: TemplateChild<ListBox>,

        settings: Settings,

        #[property(get, set)]
        client: RefCell<Client>,
    }

    #[gtk::template_callbacks]
    impl FilterDialog {
        fn init_settings(&self) {
            self.settings
                .bind(
                    "remove-short-videos",
                    &self.switch_hide_shorts.get(),
                    "active",
                )
                .flags(SettingsBindFlags::DEFAULT)
                .build();
            self.settings
                .bind(
                    "only-videos-yesterday",
                    &self.switch_hide_today.get(),
                    "active",
                )
                .flags(SettingsBindFlags::DEFAULT)
                .build();
            self.settings
                .bind(
                    "filter-enabled",
                    &self.label_filters_disabled.get(),
                    "visible",
                )
                .flags(SettingsBindFlags::GET | SettingsBindFlags::INVERT_BOOLEAN)
                .build();
        }

        pub(super) fn init_custom_filters(&self) {
            let obj = self.obj();
            let client = obj.client();
            let filters = client.filter_store();

            filters.connect_items_changed(clone!(
                #[weak(rename_to = list)]
                self.custom_filter_box,
                move |model, _, _, _| {
                    list.set_visible(model.n_items() != 0);
                }
            ));

            self.custom_filter_box.bind_model(
                Some(&filters),
                clone!(
                    #[weak]
                    client,
                    #[upgrade_or_panic]
                    move |filter| {
                        let row = ActionRow::new();
                        filter
                            .bind_property("title", &row, "subtitle")
                            .sync_create()
                            .build();
                        filter
                            .bind_property("channel", &row, "title")
                            .sync_create()
                            .build();
                        row.add_css_class("property");

                        let remove_button = Button::from_icon_name("user-trash-symbolic");
                        remove_button.set_valign(Align::Center);
                        remove_button.add_css_class("flat");
                        remove_button.connect_clicked(clone!(
                            #[weak]
                            filter,
                            #[weak]
                            client,
                            move |_| {
                                client.toggle_filter(
                                    filter
                                        .dynamic_cast()
                                        .expect("`FilterStore` to only contain `Filter`s"),
                                );
                            }
                        ));
                        row.add_suffix(&remove_button);

                        row.into()
                    }
                ),
            )
        }

        #[template_callback]
        fn add_filter(&self) {
            let title = self.entry_title.text();
            let channel = self.entry_channel.text();

            self.client
                .borrow()
                .toggle_filter(Filter::new("".into(), title, channel));

            self.entry_title.set_text("");
            self.entry_channel.set_text("");
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for FilterDialog {
        const NAME: &'static str = "TFFilterDialog";
        type Type = super::FilterDialog;
        type ParentType = adw::PreferencesDialog;

        fn new() -> Self {
            Self {
                label_filters_disabled: Default::default(),
                switch_hide_shorts: Default::default(),
                switch_hide_today: Default::default(),
                entry_title: Default::default(),
                entry_channel: Default::default(),
                custom_filter_box: Default::default(),
                settings: Settings::new(crate::config::APP_ID),
                client: Default::default(),
            }
        }

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            Self::bind_template_callbacks(klass);
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for FilterDialog {
        fn constructed(&self) {
            self.parent_constructed();
            self.init_settings();
        }
    }
    impl WidgetImpl for FilterDialog {}
    impl PreferencesDialogImpl for FilterDialog {}
    impl AdwDialogImpl for FilterDialog {}
}
