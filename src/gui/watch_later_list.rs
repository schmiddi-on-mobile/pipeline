gtk::glib::wrapper! {
    pub struct WatchLaterList(ObjectSubclass<imp::WatchLaterList>)
        @extends adw::Bin, gtk::Widget,
        @implements gtk::gio::ActionGroup, gtk::gio::ActionMap, gtk::Accessible, gtk::Buildable,
            gtk::ConstraintTarget;
}

pub mod imp {
    use adw::subclass::bin::BinImpl;
    use glib::clone;
    use glib::closure;
    use glib::closure_local;
    use glib::subclass::InitializingObject;
    use glib::subclass::Signal;
    use glib::Object;
    use glib::Properties;
    use gtk::glib;
    use gtk::prelude::*;
    use gtk::subclass::prelude::*;
    use gtk::GridView;
    use gtk::SignalListItemFactory;
    use gtk::Widget;
    use once_cell::sync::Lazy;
    use std::cell::RefCell;

    use gtk::CompositeTemplate;

    use crate::backend::Client;
    use crate::backend::PlayType;
    use crate::backend::Video;
    use crate::backend::WatchLaterStore;
    use crate::gui::utility::Utility;
    use crate::gui::video_item::VideoItem;

    #[derive(CompositeTemplate, Properties, Default)]
    #[template(resource = "/ui/watch_later_list.ui")]
    #[properties(wrapper_type = super::WatchLaterList)]
    pub struct WatchLaterList {
        #[template_child]
        list: TemplateChild<GridView>,

        #[property(get, set = Self::set_model)]
        model: RefCell<Option<WatchLaterStore>>,
        #[property(get, set, nullable)]
        selected: RefCell<Option<Video>>,

        #[property(get, set)]
        client: RefCell<Client>,
    }

    impl WatchLaterList {
        fn set_model(&self, model: WatchLaterStore) {
            self.obj()
                .property_expression("selected")
                .bind(&model, "selected", Widget::NONE);
            self.model.borrow_mut().replace(model);
        }

        pub(super) fn setup(&self) {
            let obj = self.obj();

            let factory = SignalListItemFactory::new();
            factory.connect_setup(clone!(
                #[strong]
                obj,
                move |_, list_item| {
                    let list_item = list_item.downcast_ref::<gtk::ListItem>().unwrap();
                    let video_item = VideoItem::new();
                    list_item.set_child(Some(&video_item));

                    list_item
                        .property_expression("item")
                        .bind(&video_item, "video", Widget::NONE);
                    obj.property_expression("client")
                        .bind(&video_item, "client", Widget::NONE);
                    video_item
                        .property_expression("video")
                        .chain_closure::<Option<String>>(closure!(
                            |_: Option<Object>, video: Option<Video>| {
                                video.map(Utility::accessible_description_for_video)
                            }
                        ))
                        .bind(list_item, "accessible-label", Widget::NONE);
                    video_item.connect_closure(
                        "play-video",
                        false,
                        closure_local!(
                            #[strong]
                            obj,
                            move |_: VideoItem, video: Video, play_type: PlayType| {
                                obj.emit_by_name::<()>("play-video", &[&video, &play_type]);
                            }
                        ),
                    );
                    video_item.connect_closure(
                        "copy-video-url",
                        false,
                        closure_local!(
                            #[strong]
                            obj,
                            move |_: VideoItem, video: Video| {
                                obj.emit_by_name::<()>("copy-video-url", &[&video]);
                            }
                        ),
                    );
                    video_item.connect_closure(
                        "download",
                        false,
                        closure_local!(
                            #[strong]
                            obj,
                            move |_: VideoItem, video: Video| {
                                obj.emit_by_name::<()>("download", &[&video]);
                            }
                        ),
                    );
                }
            ));
            self.list.set_factory(Some(&factory));

            self.list.set_single_click_activate(true);
            self.list.connect_activate(clone!(
                #[strong]
                obj,
                move |list_view, position| {
                    let model = list_view.model().expect("The model has to exist.");
                    let video = model
                        .item(position)
                        .expect("The item has to exist.")
                        .downcast::<Video>()
                        .expect("The item has to be an `Video`.");

                    obj.emit_by_name::<()>("play-video", &[&video, &PlayType::Default]);
                }
            ));
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for WatchLaterList {
        const NAME: &'static str = "TFWatchLaterList";
        type Type = super::WatchLaterList;
        type ParentType = adw::Bin;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for WatchLaterList {
        fn constructed(&self) {
            self.parent_constructed();
            self.setup();
        }

        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
                vec![
                    Signal::builder("play-video")
                        .param_types([Video::static_type(), PlayType::static_type()])
                        .build(),
                    Signal::builder("copy-video-url")
                        .param_types([Video::static_type()])
                        .build(),
                    Signal::builder("download")
                        .param_types([Video::static_type()])
                        .build(),
                ]
            });
            SIGNALS.as_ref()
        }
    }

    impl WidgetImpl for WatchLaterList {}
    impl BinImpl for WatchLaterList {}
}
