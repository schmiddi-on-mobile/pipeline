use gdk::gdk_pixbuf::Pixbuf;
use gdk::gio::{Cancellable, File, MemoryInputStream, SettingsBindFlags};
use gdk::prelude::{SettingsExt, SettingsExtManual};
use glib::object::ObjectExt;
use glib::{clone, Bytes};
use glib::{subclass::types::ObjectSubclassIsExt, Object};
use pcore::{ChannelId, Provider, VideoId};
use tokio::sync::MutexGuard;
use url::Url;

use crate::store::{import, legacy, Cache, Store};
use crate::{gspawn, tspawn};

use super::{Channel, Filter, SearchItem, SearchStream, Video, VideoFetcherStream};

gtk::glib::wrapper! {
    pub struct Client(ObjectSubclass<imp::Client>);
}

impl std::default::Default for Client {
    fn default() -> Self {
        Self::new()
    }
}

impl Client {
    pub fn new() -> Client {
        Object::builder().build()
    }

    pub fn setup(&self) -> Result<(), Box<dyn std::error::Error>> {
        let client = reqwest::Client::builder()
            .user_agent(format!(
                "{}/{}",
                env!("CARGO_PKG_NAME"),
                env!("CARGO_PKG_VERSION")
            ))
            .build()
            .expect("Failed to build reqwest client");

        self.imp().reqwest_client.borrow_mut().replace(client);

        let mut sqlite_path = glib::user_data_dir();
        sqlite_path.push("tubefeeder");
        sqlite_path.push("data.sqlite");
        let store = Store::new(sqlite_path)?;
        self.imp().store.borrow_mut().replace(store);
        self.load_data()?;

        let mut cache_path = glib::user_cache_dir();
        cache_path.push("tubefeeder");
        let cache = Cache::new(cache_path);
        self.imp().cache.borrow_mut().replace(cache);

        let settings = &self.imp().settings;
        settings.connect_changed(
            Some("piped-api-urls"),
            clone!(
                #[weak(rename_to = s)]
                self,
                move |_, _| {
                    gspawn!(async move { s.update_internal().await });
                }
            ),
        );
        settings.connect_changed(
            Some("peertube-search-api"),
            clone!(
                #[weak(rename_to = s)]
                self,
                move |_, _| {
                    gspawn!(async move { s.update_internal().await });
                }
            ),
        );
        settings
            .bind("only-videos-yesterday", &self.filter_store(), "hide-today")
            .flags(SettingsBindFlags::DEFAULT)
            .build();
        settings
            .bind("remove-short-videos", &self.filter_store(), "hide-shorts")
            .flags(SettingsBindFlags::DEFAULT)
            .build();
        settings
            .bind("filter-enabled", &self.filter_store(), "enabled")
            .flags(SettingsBindFlags::DEFAULT)
            .build();

        gspawn!(clone!(
            #[strong(rename_to = s)]
            self,
            async move { s.update_internal().await }
        ));

        Ok(())
    }

    pub fn cleanup(&self) -> Result<(), Box<dyn std::error::Error>> {
        let mut binding = self.imp().store.borrow_mut();
        let store = binding.as_mut().expect("Store to be set up");

        store.trim_unused()?;

        let mut binding = self.imp().cache.borrow_mut();
        let cache = binding.as_mut().expect("Store to be set up");

        cache.purge_old();

        Ok(())
    }

    fn load_data(&self) -> Result<(), Box<dyn std::error::Error>> {
        let mut binding = self.imp().store.borrow_mut();
        let store = binding.as_mut().expect("Store to be set up");

        let mut subscriptions = store.subscriptions()?;
        if subscriptions.is_empty() {
            subscriptions = legacy::load_subscriptions().unwrap_or_default();
        }

        let mut watch_later = store.watch_later_list()?;
        if watch_later.is_empty() {
            watch_later = legacy::load_watch_later().unwrap_or_default();
        }
        let mut filters = store.filters()?;
        if filters.is_empty() {
            filters = legacy::load_filters().unwrap_or_default();
        }

        drop(binding);

        for vid in watch_later {
            self.toggle_watch_later(self.construct_video(vid));
        }
        for sub in subscriptions {
            self.toggle_subscription(self.construct_channel(sub));
        }
        for fil in filters {
            self.toggle_filter(fil);
        }

        Ok(())
    }

    async fn update_internal(&self) {
        let settings = &self.imp().settings;
        let piped_apis: Vec<String> = settings.get("piped-api-urls");
        let peertube_search_api: String = settings.get("peertube-search-api");

        // TODO: Some kind of error when anything is not a valid URL?

        let client = self
            .imp()
            .reqwest_client
            .borrow()
            .clone()
            .expect("Client to be set up");
        let piped = ppiped::PipedProvider::new(
            client.clone(),
            piped_apis
                .into_iter()
                .flat_map(|u| Url::parse(&u).ok())
                .collect(),
        );
        let peertube = ppeertube::PeertubeProvider::new(
            client,
            Url::parse(&peertube_search_api).unwrap_or_else(|_| {
                Url::parse("https://sepiasearch.org/").expect("Sepiasearch to be valid URL")
            }),
        );

        let mut internal = self.internal().await;

        internal.register(piped);
        internal.register(peertube);
    }

    async fn internal(&self) -> MutexGuard<'_, papi::PipelineApi> {
        // TODO: May panic if multiple reloads/searches in parallel.
        self.imp().internal.lock().await
    }

    async fn bytes_for_url(&self, url: Url) -> Result<Bytes, reqwest::Error> {
        let client = self
            .imp()
            .reqwest_client
            .borrow()
            .clone()
            .expect("Client to be set up");
        log::trace!("Fetching data for url {}", url);
        tspawn!(async move { client.get(url).send().await?.bytes().await })
            .await
            .expect("Failed to spawn tokio")
            .map(Bytes::from_owned)
    }

    async fn pixbuf_for_url(&self, url: Url) -> Option<Pixbuf> {
        let bytes = self.bytes_for_url(url).await.ok()?;
        let stream = MemoryInputStream::from_bytes(&bytes);
        Pixbuf::from_stream(&stream, None::<&Cancellable>).ok()
    }

    fn populate_avatar(&self, channel: &Channel) {
        gspawn!(clone!(
            #[strong]
            channel,
            #[strong(rename_to = s)]
            self,
            async move {
                if let Some(url) = channel.avatar_url() {
                    let cache = s.imp().cache.borrow().clone();
                    if let Some(pixbuf) = cache
                        .as_ref()
                        .expect("Cache to be set up")
                        .get_or_fetch(url, |u| async { s.pixbuf_for_url(u).await })
                        .await
                    {
                        channel.set_avatar(pixbuf);
                    }
                }
            }
        ));
    }

    fn populate_thumbnail(&self, video: &Video) {
        gspawn!(clone!(
            #[strong]
            video,
            #[strong(rename_to = s)]
            self,
            async move {
                if let Some(url) = video.thumbnail_url() {
                    let cache = s.imp().cache.borrow().clone();
                    if let Some(pixbuf) = cache
                        .as_ref()
                        .expect("Cache to be set up")
                        .get_or_fetch(url, |u| async { s.pixbuf_for_url(u).await })
                        .await
                    {
                        video.set_thumbnail(pixbuf);
                    }
                }
            }
        ));
    }

    pub(super) fn construct_channel(&self, channel: pcore::Channel) -> Channel {
        let mut cache = self.imp().channel_cache.borrow_mut();
        if let Some(cached) = cache.get(&channel.id).and_then(|r| r.upgrade()) {
            if cached.channel() != channel {
                let requires_avatar_update = cached.avatar_url() != channel.avatar;

                cached.update(channel);

                let mut binding = self.imp().store.borrow_mut();
                let store = binding.as_mut().expect("Store to be set up");
                // TODO: Error handling
                let _ = store.update_channel_if_exists(&cached.channel());

                if requires_avatar_update {
                    self.populate_avatar(&cached);
                }
            }

            cached
        } else {
            let id = channel.id.clone();
            let object = Channel::new(channel);
            cache.insert(id, object.downgrade());
            self.populate_avatar(&object);
            object
        }
    }

    pub(super) fn construct_video(&self, video: pcore::Video) -> Video {
        let mut cache = self.imp().video_cache.borrow_mut();
        if let Some(cached) = cache.get(&video.id).and_then(|r| r.upgrade()) {
            if cached.video() != video {
                let requires_thumbnail_update = cached.thumbnail_url() != video.thumbnail;

                cached.update(video);

                let mut binding = self.imp().store.borrow_mut();
                let store = binding.as_mut().expect("Store to be set up");
                // TODO: Error handling
                let _ = store.update_video_if_exists(&cached.video());

                if requires_thumbnail_update {
                    self.populate_thumbnail(&cached);
                }
            }

            cached
        } else {
            let id = video.id.clone();
            let uploader = video.uploader.clone();
            let object = Video::new(video, &self.construct_channel(uploader));
            cache.insert(id, object.downgrade());
            self.populate_thumbnail(&object);
            object
        }
    }

    pub(super) fn construct_search_item(&self, item: pcore::SearchItem) -> SearchItem {
        match item {
            pcore::SearchItem::Video(v) => SearchItem::for_video(&self.construct_video(v)),
            pcore::SearchItem::Channel(c) => SearchItem::for_channel(&self.construct_channel(c)),
        }
    }

    pub async fn videos_for_channels(&self, channel_ids: &[ChannelId]) -> VideoFetcherStream {
        VideoFetcherStream::new(self.internal().await.videos_for_channels(channel_ids), self)
    }

    pub async fn search(&self, query: &str) -> SearchStream {
        SearchStream::new(self.internal().await.search(query), self)
    }

    pub async fn populate_video_information(&self, video_id: &VideoId) -> Result<(), pcore::Error> {
        let internal = self.imp().internal.clone();
        let video_id = video_id.clone();
        let core_video =
            tspawn!(async move { internal.lock().await.video_information(&video_id).await })
                .await
                .expect("Failed to spawn tokio")?;
        // Already in cache
        self.construct_video(core_video);
        Ok(())
    }

    pub async fn populate_channel_information(
        &self,
        channel_id: &ChannelId,
    ) -> Result<(), pcore::Error> {
        let internal = self.imp().internal.clone();
        let channel_id = channel_id.clone();
        let core_channel =
            tspawn!(async move { internal.lock().await.channel_information(&channel_id).await })
                .await
                .expect("Failed to spawn tokio")?;
        // Already in cache
        self.construct_channel(core_channel);
        Ok(())
    }

    pub fn toggle_subscription(&self, channel: Channel) {
        let now_subscribed = self.subscriptions_store().toggle(channel.clone());
        channel.set_subscribed(now_subscribed);

        let mut binding = self.imp().store.borrow_mut();
        let store = binding.as_mut().expect("Store to be set up");
        // TODO: Error handling?
        if now_subscribed {
            let _ = store.subscribe(&channel.channel());
        } else {
            let _ = store.unsubscribe(&channel.channel());
        }
    }

    pub fn toggle_watch_later(&self, video: Video) {
        let now_watch_later = self.watch_later_store().toggle(video.clone());
        video.set_watch_later(now_watch_later);

        let mut binding = self.imp().store.borrow_mut();
        let store = binding.as_mut().expect("Store to be set up");
        // TODO: Error handling?
        if now_watch_later {
            let _ = store.watch_later(&video.video());
        } else {
            let _ = store.remove_watch_later(&video.video());
        }
    }

    pub fn toggle_filter(&self, filter: Filter) {
        let now_active = self.filter_store().toggle(filter.clone());

        let mut binding = self.imp().store.borrow_mut();
        let store = binding.as_mut().expect("Store to be set up");
        // TODO: Error handling?
        if now_active {
            let _ = store.add_filter(&filter);
        } else {
            let _ = store.remove_filter(&filter);
        }
    }

    pub fn subscriptions(&self) -> Vec<Channel> {
        self.subscriptions_store().channels()
    }

    pub fn import_youtube(&self, file: File) -> Result<(), Box<dyn std::error::Error>> {
        let channels = import::from_youtube_csv(file)?;
        self.import_channels(channels);

        Ok(())
    }

    pub fn import_newpipe(&self, file: File) -> Result<(), Box<dyn std::error::Error>> {
        let channels = import::from_newpipe_json(file)?;
        self.import_channels(channels);

        Ok(())
    }

    fn import_channels(&self, channels: Vec<pcore::Channel>) {
        for channel in channels {
            let channel = self.construct_channel(channel);
            self.subscriptions_store()
                .add_if_not_exists(channel.clone());
            channel.set_subscribed(true);

            let mut binding = self.imp().store.borrow_mut();
            let store = binding.as_mut().expect("Store to be set up");
            let _ = store.subscribe(&channel.channel());
        }
    }
}

mod imp {
    use gdk::gio::Settings;
    use gdk::prelude::ObjectExt;
    use gdk::subclass::prelude::{DerivedObjectProperties, ObjectImpl, ObjectSubclass};
    use glib::{Properties, WeakRef};
    use gtk::glib;
    use std::{cell::RefCell, collections::HashMap, sync::Arc};
    use tokio::sync::Mutex;

    use crate::backend::{Channel, FilterStore, SubscriptionsStore, Video, WatchLaterStore};
    use crate::store::{Cache, Store};

    #[derive(Properties)]
    #[properties(wrapper_type = super::Client)]
    pub struct Client {
        pub(super) internal: Arc<Mutex<papi::PipelineApi>>,
        pub(super) store: RefCell<Option<Store>>,
        pub(super) cache: RefCell<Option<Cache>>,

        pub(super) reqwest_client: RefCell<Option<reqwest::Client>>,

        pub(super) channel_cache: RefCell<HashMap<pcore::ChannelId, WeakRef<Channel>>>,
        pub(super) video_cache: RefCell<HashMap<pcore::VideoId, WeakRef<Video>>>,

        #[property(get, set)]
        pub(super) subscriptions_store: RefCell<SubscriptionsStore>,
        #[property(get, set)]
        pub(super) watch_later_store: RefCell<WatchLaterStore>,
        #[property(get, set)]
        pub(super) filter_store: RefCell<FilterStore>,

        pub(super) settings: Settings,
    }

    impl Default for Client {
        fn default() -> Self {
            Self {
                internal: Default::default(),
                store: Default::default(),
                cache: Default::default(),
                reqwest_client: Default::default(),
                channel_cache: Default::default(),
                video_cache: Default::default(),
                subscriptions_store: Default::default(),
                watch_later_store: Default::default(),
                filter_store: Default::default(),
                settings: Settings::new(crate::config::APP_ID),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Client {
        const NAME: &'static str = "TFClient";
        type Type = super::Client;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Client {}
}
