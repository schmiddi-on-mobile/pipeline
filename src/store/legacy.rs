use std::fs::OpenOptions;

use chrono::{DateTime, NaiveDateTime, TimeDelta, Utc};
use csv::ReaderBuilder;
use pcore::{Channel, ChannelId, Provider, Video, VideoId};
use url::Url;

use crate::backend::Filter;

pub fn load_subscriptions() -> Result<Vec<Channel>, Box<dyn std::error::Error>> {
    let mut subscriptions_file_path = glib::user_data_dir();
    subscriptions_file_path.push("tubefeeder");
    subscriptions_file_path.push("subscriptions.csv");

    let file = OpenOptions::new()
        .read(true)
        .write(false)
        .open(&subscriptions_file_path)?;

    let csv_reader = ReaderBuilder::new()
        .has_headers(false)
        .flexible(true)
        .from_reader(file);

    let mut result = vec![];
    for record in csv_reader.into_records() {
        let record = record?;
        let platform = &record[0];
        let id = &record[1];
        let host = record
            .get(2)
            .and_then(|u| Url::parse(u).ok())
            .and_then(|u| u.host_str().map(|s| s.to_owned()));

        let id = if let Some(host) = host {
            format!("{}@{}", id, host)
        } else {
            id.to_owned()
        };

        let platform = match platform {
            "youtube" => ppiped::PipedProvider::platform_id(),
            "peertube" => ppeertube::PeertubeProvider::platform_id(),
            _ => continue,
        };

        let channel = Channel {
            id: ChannelId {
                id: id.to_owned(),
                platform,
            },
            name: Default::default(),
            avatar: Default::default(),
            banner: Default::default(),
            description: Default::default(),
            subscribers: Default::default(),
        };
        result.push(channel);
    }

    Ok(result)
}

pub fn load_watch_later() -> Result<Vec<Video>, Box<dyn std::error::Error>> {
    let mut watch_later_file_path = glib::user_data_dir();
    watch_later_file_path.push("tubefeeder");
    watch_later_file_path.push("playlist_watch_later.csv");

    let file = OpenOptions::new()
        .read(true)
        .write(false)
        .open(&watch_later_file_path)?;

    let csv_reader = ReaderBuilder::new()
        .has_headers(false)
        .flexible(true)
        .from_reader(file);

    let mut result = vec![];
    for record in csv_reader.into_records() {
        let record = record?;
        let platform = &record[0];
        let url = Url::parse(&record[1])?;
        let title = record[2].to_string();
        let uploaded: DateTime<Utc> = NaiveDateTime::parse_from_str(&record[3], "%FT%T")?.and_utc();
        let channel_name = &record[4];
        let channel_id = &record[5];
        let (host, thumbnail_url, duration) = match platform {
            "youtube" => {
                let thumbnail_url = Url::parse(&record[6])?;
                let duration = record
                    .get(7)
                    .and_then(|s| s.parse().ok())
                    .map(TimeDelta::seconds);
                (None, thumbnail_url, duration)
            }
            "peertube" => {
                let host = Url::parse(&record[6])?.host_str().map(|s| s.to_owned());
                let thumbnail_url = Url::parse(&record[7])?;
                let duration = record
                    .get(8)
                    .and_then(|s| s.parse().ok())
                    .map(TimeDelta::seconds);
                (host, thumbnail_url, duration)
            }
            _ => continue,
        };

        let channel_id = if let Some(host) = &host {
            format!("{}@{}", channel_id, host)
        } else {
            channel_id.to_owned()
        };

        let id = match platform {
            "youtube" => url
                .query_pairs()
                .find(|(query, _)| query == "v")
                .map(|(_, id)| id.into_owned())
                .ok_or("cannot parse video id")?,
            "peertube" => url
                .path_segments()
                .and_then(|s| s.last())
                .ok_or("cannot parse video id")?
                .to_owned(),
            _ => continue,
        };
        let id = if let Some(host) = &host {
            format!("{}@{}", id, host)
        } else {
            id
        };

        let platform = match platform {
            "youtube" => ppiped::PipedProvider::platform_id(),
            "peertube" => ppeertube::PeertubeProvider::platform_id(),
            _ => continue,
        };

        let channel = Channel {
            id: ChannelId {
                id: channel_id.to_owned(),
                platform: platform.clone(),
            },
            name: channel_name.to_string(),
            avatar: Default::default(),
            banner: Default::default(),
            description: Default::default(),
            subscribers: Default::default(),
        };

        let video = Video {
            id: VideoId { id, platform },
            playable_url: url,
            uploader: channel,
            title,
            uploaded,
            duration,
            thumbnail: Some(thumbnail_url),
            description: Default::default(),
            views: Default::default(),
            likes: Default::default(),
            dislikes: Default::default(),
            is_live: Default::default(),
        };
        result.push(video);
    }

    Ok(result)
}

pub fn load_filters() -> Result<Vec<Filter>, Box<dyn std::error::Error>> {
    let mut filters_file_path = glib::user_data_dir();
    filters_file_path.push("tubefeeder");
    filters_file_path.push("filters.csv");

    let file = OpenOptions::new()
        .read(true)
        .write(false)
        .open(&filters_file_path)?;

    let csv_reader = ReaderBuilder::new()
        .has_headers(false)
        .flexible(true)
        .from_reader(file);

    let mut result = vec![];
    for record in csv_reader.into_records() {
        let record = record?;
        let platform = &record[0];
        let title = &record[1];
        let channel = &record[2];

        let filter = Filter::new(platform, title, channel);
        result.push(filter);
    }

    Ok(result)
}
