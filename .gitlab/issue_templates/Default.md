<!-- Before opening an issue, please check a few things:

- Please check if a similar issue already exists.
- Are YouTube subscriptions not working? This may be the fault of the Piped instance. Please check if setting another instance (in the settings) fixes your issue.
- Is it an issue with the built-in video player? Please check if you can reproduce the issue with [Clapper](https://github.com/Rafostar/clapper); if yes, then open an issue with that project.

-->

## Description

<!-- Insert description here -->

## Additional information

Installation method: <!-- Probably Flatpak -->

Version: <!-- From the about page -->

Additional information: <!-- Put any remaining information here -->
