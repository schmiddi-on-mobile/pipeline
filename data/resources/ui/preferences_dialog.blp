using Gtk 4.0;
using Adw 1;

template $TFPreferencesDialog: Adw.PreferencesDialog {
  Adw.PreferencesPage {
    title: _("General");
    icon-name: 'preferences-system-symbolic';

    Adw.PreferencesGroup {
      title: _("General");

      Adw.SwitchRow switch_reload_on_startup {
        title: _("Reload on Startup");
      }
    }

    Adw.PreferencesGroup {
      title: _("External Programs");

      Adw.SwitchRow switch_external_default {
        title: _("Play with External Player by Default");
      }

      Adw.EntryRow entry_external_player {
        title: _("External Video Player");
        show-apply-button: true;
        input-hints: no_spellcheck | no_emoji;
        apply => $apply_external_player() swapped;
      }

      Adw.EntryRow entry_external_downloader {
        title: _("External Video Downloader");
        show-apply-button: true;
        input-hints: no_spellcheck | no_emoji;
        apply => $apply_external_downloader() swapped;
      }
    }

    Adw.PreferencesGroup {
      title: _("Piped APIs");
      description: _("The Piped instances Pipeline will use to fetch YouTube videos from. The first instance will be primarily used, while the other instances are fallbacks in case the previous instances all failed. For a complete list of public APIs, see the <a href=\"https://github.com/TeamPiped/Piped/wiki/Instances\">Instances page</a>.");

      ListBox piped_api_urls {
        styles [
          "boxed-list",
        ]

        selection-mode: none;
      }
    }

    Adw.PreferencesGroup {
      title: _("Peertube Preferences");

      Adw.EntryRow entry_peertube_search_api {
        title: _("Search Instance");
        show-apply-button: true;
        input-purpose: url;
        input-hints: no_spellcheck | lowercase | no_emoji;
        apply => $apply_peertube_search_api() swapped;
      }
    }
  }
}
