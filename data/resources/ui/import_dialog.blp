using Gtk 4.0;
using Adw 1;

template $TFImportDialog: Adw.Dialog {
  content-width: 400;
  content-height: 210;

  child: Adw.ToolbarView {
    [top]
    Adw.HeaderBar {
      title-widget: Adw.ViewSwitcher {
        stack: platform_stack;
      };
    }

    Gtk.ScrolledWindow {
      hscrollbar-policy: never;

      Adw.ViewStack platform_stack {
        Adw.ViewStackPage {
          name: "youtube";
          icon-name: "youtube";
          title: "YouTube";

          child: Gtk.Box {
            orientation: vertical;
            margin-top: 6;
            margin-bottom: 12;
            margin-start: 12;
            margin-end: 12;

            Gtk.Label {
              label: _("To import subscriptions from YouTube, you need to export your subscriptions as a CSV from <a href=\"https://takeout.google.com/settings/takeout/downloads\">Google Takeout</a>. Afterwards, select the downloaded file to import it into Pipeline.");
              wrap: true;
              use-markup: true;
              justify: center;
              vexpand: true;
            }

            Gtk.Button {
              styles [
                "pill",
                "suggested-action",
              ]

              label: _("Import from YouTube");
              valign: end;
              vexpand: true;
              halign: center;
              clicked => $import_youtube() swapped;
            }
          };
        }

        Adw.ViewStackPage {
          name: "newpipe";
          icon-name: "newpipe";
          title: "NewPipe";

          child: Gtk.Box {
            orientation: vertical;
            margin-top: 6;
            margin-bottom: 12;
            margin-start: 12;
            margin-end: 12;

            Gtk.Label {
              label: _("To import subscriptions from NewPipe, you need to export your subscriptions to a JSON file. Afterwards, select the downloaded file to import it into Pipeline.");
              wrap: true;
              justify: center;
              vexpand: true;
            }

            Gtk.Button {
              styles [
                "pill",
                "suggested-action",
              ]

              label: _("Import from NewPipe");
              valign: end;
              vexpand: true;
              halign: center;
              clicked => $import_newpipe() swapped;
            }
          };
        }
      }
    }
  };
}
