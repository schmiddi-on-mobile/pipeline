# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.1.1] - 2025-01-30

### Fixed

- Buttons and some video information being cut off in the video page on small height.

## [2.1.0] - 2024-12-05

### Changed

- Major UI improvements to the video page.
- Major UI improvements to the channel page.
- Minor UI improvements to duration indicators.

### Fixed

- Start with the watch-later list at the top instead of the bottom.
- Long video and channel names breaking window size on small screens.
- Wrong background color for video list and channel list.

## [2.0.3] - 2024-11-02

### Changed

- Improved import dialogs.
- Make the search bar wider.
- Improved error dialog.
- Show that the app is loading when starting the app.

### Fixed

- Panic for migrating watch-later list to the new version for some YouTube videos.
- Importing NewPipe videos not showing the file to import.

### Chores

- Updated to GNOME 47 runtimes.

## [2.0.2] - 2024-09-14

### HOTFIX

- Bad performance of the built-in video player.

### Added

- Ignore Piped error when opening a video.
- Don't show video title in player when not fullscreened.

## [2.0.1] - 2024-09-12

### Fixed

- Infinite loading on channels with no videos.
- Video not stopping when displaying channel in the subscriptions tab.
- Video player not centered on fullscreen when video aspect ratio does not match display aspect ratio.
- Slight redesign of channel page to fix video widgets growing in size when scrolling.
- Swiping not pausing video and exiting fullscreen.
- UI not fitting in window with certain widths.

## [2.0.0] - 2024-09-10

A complete rewrite of the application, in order to allow for easier maintenance, more features and a better user experience.

### Added

- Search for videos and channels.
- Play videos inside the application using [Clapper](https://github.com/Rafostar/clapper).
- Show toast when the video URL was copied.
- Import PeerTube subscriptions from NewPipe
- Allow for backup Piped instances.
- Option to not automatically refresh when started.
- Infinite scrolling of the feed.

### Fixed

- Many videos not showing up in the feed anymore. Workaround for an [upstream Piped issue](https://github.com/TeamPiped/Piped/issues/3761).

## Unreleased pre-2.0.0

### Fix

- Not grabbing focus for the entry when adding a subscription.

## [1.15.0] - 2024-05-28

### Fix

- Crash with certain channels.

### Chores

- Updated to GNOME 46 libraries.

## [1.14.5] - 2024-02-24

### HOTFIX

- Subscription names not showing up in the subscription list anymore.

## [1.14.4] - 2024-02-22

### Added

- Action to open video in the browser.

### Fixed

- Outdated window title
- Wrong offset of the reload-spinner in relation to the reload-button.

### Changed

- Updated selected Piped instance list.

## [1.14.3] - 2024-01-11

### Added

- "Livi" video player in the list of predefined video players.
- Option to hide all short videos.

## [1.14.2] - 2023-12-08

### Added

- Video title as tooltip of the video.
- Video title in the video information screen.
- Error handling when failing to add a subscription.
- Error handling when failing to play or download videos.

## [1.14.1] - 2023-11-11

### Added

- Keyboard shortcuts for actions.

### Fixed

- Video duration placed out-of-bounds if thumbnail does not load.

### Chores

- Updated dependencies.

## [1.14.0] - 2023-10-14

### Added

- A dropdown for some popular video players.
- A dropdown for some popular Piped APIs.

### Changed

- Removed buttons in the list in favor of a menu shown on right-click or long-press (touch-screen only). 
- Updated to GTK 4.12 and Libadwaita 1.4.
- Use GridView instead of ListView for all pages.
- Improvements regarding UI in the add-subscription dialog.
- The button on the empty feed page will now lead to the subscription page instead of directly adding a subscription.

### Fixed

- Inconsistent size of video thumbnails.

## [1.13.1] - 2023-08-24

### Fixed

- Copy video URL not working anymore.
- Missing accesibility labels.

## [1.13.0] - 2023-08-20

### Added

- Dialog showing video information including likes, dislikes (not for YouTube), views and video description.
- Show video duration on video thumbnails.

### Removed

- Removed Lbry support as it will have to shut down soon.

### Fixed

- File chooser dialog for importing videos not working.

[Unreleased]: https://gitlab.com/schmiddi-on-mobile/pipeline/-/compare/v2.1.1...master
[2.1.1]: https://gitlab.com/schmiddi-on-mobile/pipeline/-/compare/v2.1.0...v2.1.1
[2.1.0]: https://gitlab.com/schmiddi-on-mobile/pipeline/-/compare/v2.0.4...v2.1.0
[2.0.4]: https://gitlab.com/schmiddi-on-mobile/pipeline/-/compare/v2.0.3...v2.0.4
[2.0.3]: https://gitlab.com/schmiddi-on-mobile/pipeline/-/compare/v2.0.2...v2.0.3
[2.0.2]: https://gitlab.com/schmiddi-on-mobile/pipeline/-/compare/v2.0.1...v2.0.2
[2.0.1]: https://gitlab.com/schmiddi-on-mobile/pipeline/-/compare/v2.0.0...v2.0.1
[2.0.0]: https://gitlab.com/schmiddi-on-mobile/pipeline/-/compare/v1.15.0...v2.0.0
[1.15.0]: https://gitlab.com/schmiddi-on-mobile/pipeline/-/compare/v1.14.5...v1.15.0
[1.14.5]: https://gitlab.com/schmiddi-on-mobile/pipeline/-/compare/v1.14.4...v1.14.5
[1.14.4]: https://gitlab.com/schmiddi-on-mobile/pipeline/-/compare/v1.14.3...v1.14.4
[1.14.3]: https://gitlab.com/schmiddi-on-mobile/pipeline/-/compare/v1.14.2...v1.14.3
[1.14.2]: https://gitlab.com/schmiddi-on-mobile/pipeline/-/compare/v1.14.1...v1.14.2
[1.14.1]: https://gitlab.com/schmiddi-on-mobile/pipeline/-/compare/v1.14.0...v1.14.1
[1.14.0]: https://gitlab.com/schmiddi-on-mobile/pipeline/-/compare/v1.13.1...v1.14.0
[1.13.1]: https://gitlab.com/schmiddi-on-mobile/pipeline/-/compare/v1.13.0...v1.13.1
[1.13.0]: https://gitlab.com/schmiddi-on-mobile/pipeline/-/compare/v1.12.0...v1.13.0
