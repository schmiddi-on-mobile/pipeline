<div align="center">
 <h1>📽 Pipeline</h1>
 <strong>Follow your favorite video creators</strong>
</div>

<div class="ignore-css">
 <p align="center">
  <a href="https://flathub.org/apps/de.schmidhuberj.tubefeeder"><img src="https://img.shields.io/flathub/downloads/de.schmidhuberj.tubefeeder?logo=flathub&labelColor=77767b&color=4a90d9"></a>
  <a href="https://matrix.to/#/%23pipelineapp:matrix.org"><img src="https://img.shields.io/badge/Matrix-Join-brightgreen"></a>
  <a href="https://hosted.weblate.org/engage/schmiddi-on-mobile/"><img src="https://hosted.weblate.org/widgets/schmiddi-on-mobile/-/pipeline/svg-badge.svg"></a>
 </p>
</div>
<br>
Pipeline lets you watch and download videos from YouTube and PeerTube, all without needing to navigate through different websites. Its adaptive design allows you to enjoy content on any screen size.

![](/data/screenshots/video.png)

## Installation

<table>
  <tr>
    <td>Flatpak</td>
    <td>
      <a href='https://flathub.org/apps/details/de.schmidhuberj.tubefeeder'><img width='130' alt='Download on Flathub' src='https://flathub.org/api/badge?svg&locale=en'/></a>
    </td>
  </tr>
</table>

## Features

- Search for you favorite channels and subscribe to them.
- Aggregate videos from all subscriptions into a single feed
- Filter out unwanted items from the feed, like short videos or videos from a series.
- Play those videos either in the built-in video player or with any other video player of your choice.
- Download videos for offline viewing.
- Manage videos you want to watch later.
- Import subscriptions from [NewPipe](https://github.com/TeamNewPipe/NewPipe/) or YouTube
- Multiple platforms:
    - YouTube (using Piped as the backend to prevent throttling)
    - PeerTube
    - suggest any other platform with a good API and it will be considered

## Contributing

See the [Contributing page](CONTRIBUTING.md).

## Translation

Pipeline can easily be translated for other languages, as it uses gettext. Please consider contributing translations using [Weblate](https://hosted.weblate.org/engage/schmiddi-on-mobile/), as an alternative you can also open merge requests and I will notify you if updates are necessary. Thanks to Weblate for free hosting and all the translators for their great work keeping the translations up-to-date.

<a href="https://hosted.weblate.org/engage/schmiddi-on-mobile/">
<img src="https://hosted.weblate.org/widgets/schmiddi-on-mobile/-/pipeline/multi-auto.svg" alt="Translation status" />
</a>

## Wiki

Please also take a look at the [wiki](https://gitlab.com/schmiddi-on-mobile/pipeline/-/wikis/home) for more information on how to use the app.
