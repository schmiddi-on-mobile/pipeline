# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the tubefeeder package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: tubefeeder\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-11-06 10:41+0100\n"
"PO-Revision-Date: 2024-11-12 13:09+0000\n"
"Last-Translator: LucasMZ <git@lucasmz.dev>\n"
"Language-Team: Portuguese (Brazil) <https://hosted.weblate.org/projects/"
"schmiddi-on-mobile/pipeline/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.8.2\n"

#: data/de.schmidhuberj.tubefeeder.desktop.in.in:5
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:6
#: data/resources/ui/window.blp:9 src/gui/video_page.rs:319
msgid "Pipeline"
msgstr "Pipeline"

#: data/de.schmidhuberj.tubefeeder.desktop.in.in:6
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:7
msgid "Follow video creators"
msgstr "Siga criadores de vídeos"

#: data/de.schmidhuberj.tubefeeder.desktop.in.in:12
msgid "youtube;peertube;video;"
msgstr "youtube;peertube;video;vídeo;vídeos;"

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:331
msgid ""
"Pipeline lets you watch and download videos from YouTube and PeerTube, all "
"without needing to navigate through different websites. Its adaptive design "
"allows you to enjoy content on any screen size."
msgstr ""
"Com o Pipeline você pode assistir e baixar vídeos do YouTube e do PeerTube, "
"sem precisar navegar por diferentes páginas da web. Seu design adaptativo "
"permite que você desfrute de conteúdos em telas de qualquer tamanho."

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:336
msgid "Pipeline allows you to:"
msgstr "O Pipeline permite a você:"

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:342
msgid "Search for you favorite channels and subscribe to them."
msgstr "Pesquisar pelos seus canais favoritos e inscrever-se neles."

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:344
msgid "Aggregate the videos of all subscriptions into a single feed."
msgstr "Agregar os vídeos de todas as inscrições em um único feed."

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:346
msgid ""
"Filter out unwanted items from the feed, like short videos or videos from a "
"series."
msgstr ""
"Filtrar itens indesejáveis para fora do feed, como vídeos curtos ou vídeos "
"de uma série."

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:348
msgid ""
"Play those videos either in the built-in video player or with any other "
"video player of your choice."
msgstr ""
"Reproduzir os vídeos tanto com o reprodutor de vídeo incluído quanto com "
"qualquer outro reprodutor de sua escolha."

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:350
msgid "Download videos for offline viewing."
msgstr "Baixe vídeos para assisti-los off-line."

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:352
msgid "Manage videos you want to watch later."
msgstr "Gerenciar os vídeos que você quiser assistir mais tarde.."

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:354
msgid "Import your subscriptions from NewPipe or YouTube."
msgstr "Importar inscrições do NewPipe ou do YouTube."

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:356
msgid ""
"Supports YouTube, PeerTube with the possibility to extend to other platforms "
"on request."
msgstr ""
"Suporta o YouTube, PeerTube, com a possibilidade de se estender a outras "
"plataformas caso requisitado."

#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:362
msgid "Playing a Video"
msgstr "Reproduzindo um vídeo"

#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:366
msgid "Viewing Videos of a Channel"
msgstr "Assistindo a vídeos de um canal"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:6
msgid "Window width"
msgstr "Largura da janela"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:10
msgid "Window height"
msgstr "Altura da janela"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:14
msgid "Window maximized state"
msgstr "Estado maximizado da janela"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:18
msgid "The player to use"
msgstr "O reprodutor a ser utilizado"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:22
msgid "The downloader to use"
msgstr "O baixador a ser utilizado"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:26
msgid "The piped api url"
msgstr "A URL da API Piped"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:30
msgid "The piped api urls"
msgstr "As URLs da API Piped"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:34
msgid "The peertube api used for searches"
msgstr "A API PeerTube utilizada para pesquisas"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:38
msgid "Only show videos of yesterday."
msgstr "Mostrar somente vídeos de ontem."

#: data/de.schmidhuberj.tubefeeder.gschema.xml:42
msgid "Remove short videos."
msgstr "Remover vídeos curtos."

#: data/de.schmidhuberj.tubefeeder.gschema.xml:46
msgid "Whether the filter is enabled."
msgstr "Se o filtro está ativado."

#: data/de.schmidhuberj.tubefeeder.gschema.xml:50
msgid "The type of platform for which the last subscription was added."
msgstr "O tipo de plataforma para a qual a última inscrição foi adicionada."

#: data/de.schmidhuberj.tubefeeder.gschema.xml:54
msgid "Whether to reload when starting the application."
msgstr "Se o conteúdo deve ser recarregado ao iniciar o aplicativo."

#: data/de.schmidhuberj.tubefeeder.gschema.xml:58
msgid "Whether to play with the external player by default."
msgstr ""
"Se os vídeos devem ser reproduzidos com o reprodutor externo por padrão."

#: data/de.schmidhuberj.tubefeeder.gschema.xml:64
msgid "Stores last volume value to apply on startup"
msgstr ""
"Salva o valor de volume mais recente para aplicar ao iniciar o aplicativo"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:68
msgid "Stores last speed value to apply on startup"
msgstr ""
"Salva o valor de velocidade mais recente para aplicar ao iniciar o aplicativo"

#: data/resources/ui/channel_page.blp:63
msgid "Description"
msgstr "Descrição"

#: data/resources/ui/error_dialog.blp:12
msgid "Errors"
msgstr "Erros"

#: data/resources/ui/error_dialog.blp:20
msgid "Clear Errors"
msgstr "Limpar erros"

#: data/resources/ui/filter_dialog.blp:6
#: data/resources/ui/preferences_dialog.blp:6
#: data/resources/ui/preferences_dialog.blp:10
#: data/resources/ui/shortcuts.blp:47
msgid "General"
msgstr "Geral"

#: data/resources/ui/filter_dialog.blp:13
msgid ""
"Filters are currently disabled. Those settings will not have an effect until "
"you enable filters again."
msgstr ""
"Os filtros estão atualmente desativados. Essas configurações não surtirão "
"efeito até que você ative os filtros novamente."

#: data/resources/ui/filter_dialog.blp:18
msgid "General Filters"
msgstr "Filtros gerais"

#: data/resources/ui/filter_dialog.blp:21
msgid "Hide Short Videos"
msgstr "Ocultar vídeos curtos"

#: data/resources/ui/filter_dialog.blp:22
msgid "Hide videos shorter than one minute."
msgstr "Ocultar vídeos com menos de 1 minuto de duração."

#: data/resources/ui/filter_dialog.blp:26
msgid "Hide Videos from Today"
msgstr "Ocultar vídeos de hoje"

#: data/resources/ui/filter_dialog.blp:27
msgid "Hide videos which came out today"
msgstr "Ocultar vídeos que foram lançados hoje"

#: data/resources/ui/filter_dialog.blp:32
msgid "Custom Filters"
msgstr "Filtros personalizados"

#: data/resources/ui/filter_dialog.blp:33
msgid ""
"Filter out videos that contain a given title and were uploaded by a given "
"channel name"
msgstr ""
"Filtrar vídeos que contenham um determinado título e tenham sido publicados "
"por um determinado canal"

#: data/resources/ui/filter_dialog.blp:51
msgid "Video Title"
msgstr "Título do vídeo"

#: data/resources/ui/filter_dialog.blp:55
msgid "Channel Name"
msgstr "Nome do canal"

#: data/resources/ui/filter_dialog.blp:59
msgid "Add Filter"
msgstr "Adicionar filtro"

#: data/resources/ui/preferences_dialog.blp:13
msgid "Reload on Startup"
msgstr "Recarregar ao iniciar"

#: data/resources/ui/preferences_dialog.blp:18
msgid "External Programs"
msgstr "Programas externos"

#: data/resources/ui/preferences_dialog.blp:21
msgid "Play with External Player by Default"
msgstr "Reproduzir com reprodutor externo por padrão"

#: data/resources/ui/preferences_dialog.blp:24
msgid "External Video Player"
msgstr "Reprodutor de vídeo externo"

#: data/resources/ui/preferences_dialog.blp:30
msgid "External Video Downloader"
msgstr "Baixador de vídeo externo"

#: data/resources/ui/preferences_dialog.blp:38
msgid "Piped APIs"
msgstr "APIs Piped"

#: data/resources/ui/preferences_dialog.blp:39
msgid ""
"The Piped instances Pipeline will use to fetch YouTube videos from. The "
"first instance will be primarily used, while the other instances are "
"fallbacks in case the previous instances all failed. For a complete list of "
"public APIs, see the <a href=\"https://github.com/TeamPiped/Piped/wiki/"
"Instances\">Instances page</a>."
msgstr ""
"As instâncias Piped que o Pipeline usará para obter vídeos do YouTube. A "
"primeira instância será utilizada primariamente, enquanto as outras "
"instâncias serão opções de contingência caso todas as instâncias anteriores "
"falhem. Para uma lista completa de APIs públicas, veja a <a href=\"https://"
"github.com/TeamPiped/Piped/wiki/Instances\">página de Instâncias</a>."

#: data/resources/ui/preferences_dialog.blp:51
msgid "Peertube Preferences"
msgstr "Preferências do PeerTube"

#: data/resources/ui/preferences_dialog.blp:54
msgid "Search Instance"
msgstr "Instância de pesquisa"

#: data/resources/ui/shortcuts.blp:8
msgid "Application"
msgstr "Aplicativo"

#: data/resources/ui/shortcuts.blp:12
msgid "Bookmark Video or Subscribe to Channel"
msgstr "Favoritar vídeo ou inscrever-se no canal"

#: data/resources/ui/shortcuts.blp:17
msgid "Reload Video Feed or Channel Page"
msgstr "Recarregar feed de vídeos ou página do canal"

#: data/resources/ui/shortcuts.blp:22
msgid "Play or Pause the Video"
msgstr "Reproduzir ou pausar vídeo"

#: data/resources/ui/shortcuts.blp:23
msgid "Only when the video page is in focus"
msgstr "Somente quando a página do vídeo estiver em foco"

#: data/resources/ui/shortcuts.blp:28
msgid "Pages"
msgstr "Páginas"

#: data/resources/ui/shortcuts.blp:32
msgid "Switch to Feed Page"
msgstr "Mudar para a página de Feed"

#: data/resources/ui/shortcuts.blp:37
msgid "Switch to Watch-Later Page"
msgstr "Mudar para a página Assistir Mais Tarde"

#: data/resources/ui/shortcuts.blp:42
msgid "Switch to Subscriptions Page"
msgstr "Mudar para a página de Inscrições"

#: data/resources/ui/shortcuts.blp:52 data/resources/ui/window.blp:149
#: data/resources/ui/window.blp:305
msgid "Search"
msgstr "Pesquisar"

#: data/resources/ui/shortcuts.blp:57 data/resources/ui/window.blp:40
msgid "Go Back"
msgstr "Voltar"

#: data/resources/ui/shortcuts.blp:62
msgid "Quit"
msgstr "Sair"

#: data/resources/ui/shortcuts.blp:67
msgid "Show Shortcuts"
msgstr "Mostrar atalhos de teclado"

#: data/resources/ui/shortcuts.blp:72
msgid "Show Preferences"
msgstr "Mostrar Preferências"

#: data/resources/ui/shortcuts.blp:77
msgid "Open Menu"
msgstr "Abrir menu"

#: data/resources/ui/video_page.blp:49
msgid "Video is Playing Externally"
msgstr "O vídeo está sendo reproduzido externamente"

#: data/resources/ui/video_page.blp:175 src/gui/video_item.rs:161
msgid "Download"
msgstr "Baixar"

#: data/resources/ui/video_page.blp:193
msgid "Copy Video Link"
msgstr "Copiar link do vídeo"

#. I don't think this will ever be shown.
#: data/resources/ui/window.blp:31
msgid "Main Page"
msgstr "Página principal"

#: data/resources/ui/window.blp:57 data/resources/ui/window.blp:134
msgid "Reload"
msgstr "Recarregar"

#: data/resources/ui/window.blp:91
msgid "Video"
msgstr "Vídeo"

#: data/resources/ui/window.blp:103
msgid "Channel"
msgstr "Canal"

#. I don't think this will ever be shown.
#: data/resources/ui/window.blp:118
msgid "Sidebar"
msgstr "Barra lateral"

#: data/resources/ui/window.blp:156
msgctxt "accessibility"
msgid "Primary menu"
msgstr "Menu principal"

#: data/resources/ui/window.blp:160
msgctxt "tooltip"
msgid "Primary menu"
msgstr "Menu principal"

#: data/resources/ui/window.blp:186
msgid "Search for Videos and Channels"
msgstr "Pesquisar vídeos e canais"

#: data/resources/ui/window.blp:203
msgid "Feed"
msgstr "Feed"

#: data/resources/ui/window.blp:230 data/resources/ui/window.blp:288
msgid "No Subscriptions Yet"
msgstr "Nenhuma inscrição ainda"

#: data/resources/ui/window.blp:231 data/resources/ui/window.blp:289
msgid "Subscribe to channels first"
msgstr "Inscreva-se em canais"

#: data/resources/ui/window.blp:238
msgid "Loading Videos"
msgstr "Carregando vídeos"

#: data/resources/ui/window.blp:245 data/resources/ui/window.blp:272
msgid "No Videos"
msgstr "Nenhum vídeo"

#: data/resources/ui/window.blp:246
msgid "Try Reloading"
msgstr "Tente recarregar"

#: data/resources/ui/window.blp:255 src/gui/video_item.rs:138
msgid "Watch Later"
msgstr "Assistir Mais Tarde"

#: data/resources/ui/window.blp:273
msgid "Add videos to your watch later list to display them here"
msgstr "Adicione vídeos à sua lista Assistir Mais Tarde para mostrá-los aqui"

#: data/resources/ui/window.blp:281
msgid "Subscriptions"
msgstr "Inscrições"

#: data/resources/ui/window.blp:333
msgid "Searching"
msgstr "Pesquisando"

#: data/resources/ui/window.blp:344
msgid "Show"
msgstr "Mostrar"

#: data/resources/ui/window.blp:351
msgid "Manage Filters"
msgstr "Gerenciar filtros"

#: data/resources/ui/window.blp:357
msgid "Toggle Filters"
msgstr "Ativar/Desativar filtros"

#: data/resources/ui/window.blp:365
msgid "Import"
msgstr "Importar"

#: data/resources/ui/window.blp:369
msgid "Preferences"
msgstr "Preferências"

#: data/resources/ui/window.blp:374
msgid "Keyboard Shortcuts"
msgstr "Atalhos de teclado"

#: data/resources/ui/window.blp:378
msgid "About Pipeline"
msgstr "Sobre o Pipeline"

#: src/backend/channel.rs:87
msgid "Unknown Channel"
msgstr "Canal desconhecido"

#: src/backend/search_stream.rs:96
msgid "Error searching one platform"
msgstr "Erro ao pesquisar numa plataforma"

#: src/backend/video_fetcher_stream.rs:130
msgid "Error loading one channel"
msgid_plural "Error loading {} channels"
msgstr[0] "Erro ao carregar um canal"
msgstr[1] "Erro ao carregar {} canais"

#: src/gui/channel_item.rs:86 src/gui/window.rs:231
msgid "Unsubscribe"
msgstr "Cancelar inscrição"

#: src/gui/channel_item.rs:91 src/gui/window.rs:233
msgid "Subscribe"
msgstr "Inscrever-se"

#: src/gui/channel_page.rs:34
msgid "Failed to load channel data"
msgstr "Falha ao carregar dados do canal"

#: src/gui/channel_page.rs:249 src/gui/video_page.rs:228
msgid "one subscriber"
msgid_plural "{} subscribers"
msgstr[0] "um inscrito"
msgstr[1] "{} inscritos"

#: src/gui/preferences_dialog.rs:254
msgid "New Piped API"
msgstr "Nova API Piped"

#. Note to translators: Format numbers compactly. For numbers at least one billion.
#: src/gui/utility.rs:61
msgctxt "number-formatting"
msgid "{}B"
msgstr "{}B"

#. Note to translators: Format numbers compactly. For numbers at least one million.
#: src/gui/utility.rs:65
msgctxt "number-formatting"
msgid "{}M"
msgstr "{}M"

#. Note to translators: Format numbers compactly. For numbers at least one thousand.
#: src/gui/utility.rs:69
msgctxt "number-formatting"
msgid "{}K"
msgstr "{} mil"

#: src/gui/utility.rs:87
msgid "{title} uploaded by {channel} on {date}"
msgstr "{title} publicado por {channel} em {date}"

#. Translators: formatting of time of day in a human-readable fashion, see https://docs.rs/chrono/latest/chrono/format/strftime/index.html#specifiers
#: src/gui/utility.rs:100
msgid "%H:%M"
msgstr "%H:%M"

#: src/gui/utility.rs:102
msgid "Tomorrow"
msgstr "Amanhã"

#: src/gui/utility.rs:104
msgid "Yesterday"
msgstr "Ontem"

#. Translators: formatting of dates without year in a human-readable fashion, see https://docs.rs/chrono/latest/chrono/format/strftime/index.html#specifiers
#: src/gui/utility.rs:107
msgid "%a, %d. %B"
msgstr "%a, %d. %B"

#. Translators: formatting of dates with year in a human-readable fashion, see https://docs.rs/chrono/latest/chrono/format/strftime/index.html#specifiers
#: src/gui/utility.rs:110
msgid "%Y-%m-%d"
msgstr "%d/%m/%Y"

#: src/gui/video_item.rs:133
msgid "Remove from Watch Later"
msgstr "Remover de Assistir Mais Tarde"

#: src/gui/video_item.rs:145
msgid "Play with Internal Player"
msgstr "Reproduzir com reprodutor interno"

#: src/gui/video_item.rs:150
msgid "Play with External Player"
msgstr "Reproduzir com reprodutor externo"

#: src/gui/video_item.rs:156
msgid "Copy Video URL"
msgstr "Copiar URL do vídeo"

#: src/gui/video_page.rs:70
msgid "Failed to spawn video player"
msgstr "Falha ao abrir reprodutor de vídeo"

#: src/gui/video_page.rs:193
msgid "Failed to populate video information"
msgstr "Falha ao preencher informações do vídeo"

#: src/gui/video_page.rs:204
msgid "Failed to fetch channel information"
msgstr "Falha ao obter informações do canal"

#: src/gui/video_page.rs:237
msgid "Uploaded {}"
msgstr "Publicado {}"

#: src/gui/video_page.rs:282
msgid "Remove video from Watch Later"
msgstr "Remover vídeo do Assistir Mais Tarde"

#: src/gui/video_page.rs:284
msgid "Add video to Watch Later"
msgstr "Adicionar vídeo ao Assistir Mais Tarde"

#: src/gui/window.rs:78
msgid "One Error"
msgid_plural "{} Errors"
msgstr[0] "Um erro"
msgstr[1] "{} erros"

#: src/gui/window.rs:394
msgid "Copied URL"
msgstr "URL copiada"

#: src/gui/window.rs:420
msgid "Started Download"
msgstr "Download iniciado"

#: src/gui/window.rs:427
msgid "Failed to spawn video downloader"
msgstr "Falha ao abrir baixador de vídeo"

#: src/gui/window.rs:432
msgid "Finished Download"
msgstr "Download concluído"

#: src/gui/window.rs:680
msgid "Failed to set up application"
msgstr "Falha ao configurar aplicativo"

#~ msgid "Copy URL to Clipboard"
#~ msgstr "Copiar URL para área de transferência"

#~ msgid "Download Video"
#~ msgstr "Baixar vídeo"

#~ msgid "Remove this video from the watch-later list"
#~ msgstr "Remover este vídeo da lista Assistir Mais Tarde"

#~ msgid "Add this video to the watch-later list"
#~ msgstr "Adicionar este vídeo à lista Assistir Mais Tarde"

#~ msgid "Pipeline comes with several features:"
#~ msgstr "O Pipeline vem com diversas funções:"

#~ msgid "Subscribe to channels"
#~ msgstr "Inscreva-se em canais"

#~ msgid "Filter out unwanted videos in the feed"
#~ msgstr "Filtre vídeos indesejados para fora do feed"

#~ msgid "Multiple platforms"
#~ msgstr "Múltiplas plataformas"

#~ msgid "Feed List"
#~ msgstr "Lista de feeds"

#~ msgid "Filter List"
#~ msgstr "Lista de filtros"

#~ msgid "translator-credits"
#~ msgstr "Filipe Motta <luizfilipemotta@gmail.com>"

#~ msgid "Open in Browser"
#~ msgstr "Abrir no navegador"

#~ msgid "More Information"
#~ msgstr "Mais informações"

#~ msgid "Please check your downloader in the preferences."
#~ msgstr "Por favor, verifique o seu baixador nas Preferências."

#~ msgid "Close"
#~ msgstr "Fechar"

#~ msgid "Please check your player in the preferences."
#~ msgstr "Por favor, verifique o seu reprodutor nas Preferências."

#~ msgctxt "tooltip"
#~ msgid "Reload"
#~ msgstr "Recarregar"

#~ msgid "Your feed will appear once you've added subscriptions"
#~ msgstr "Seu feed aparecerá quando você tiver adicionado inscrições"

#~ msgid "Manage Subscriptions…"
#~ msgstr "Gerenciar inscrições…"

#~ msgid "Remove"
#~ msgstr "Remover"

#~ msgid "Filters"
#~ msgstr "Filtros"

#~ msgctxt "accessibility"
#~ msgid "Add Filter…"
#~ msgstr "Adicionar filtro…"

#~ msgctxt "tooltip"
#~ msgid "Add Filter…"
#~ msgstr "Adicionar filtro…"

#~ msgid "Add Filter…"
#~ msgstr "Adicionar filtro…"

#~ msgid "Cancel"
#~ msgstr "Cancelar"

#~ msgid "Add"
#~ msgstr "Adicionar"

#~ msgid "Import Subscriptions"
#~ msgstr "Importar inscrições"

#~ msgid ""
#~ "This will import your subscriptions from exported data from NewPipe or "
#~ "YouTube."
#~ msgstr ""
#~ "Isso importará suas inscrições a partir dos seus dados exportados do "
#~ "NewPipe ou do YouTube."

#~ msgid "NewPipe"
#~ msgstr "NewPipe"

#~ msgid "YouTube"
#~ msgstr "YouTube"

#~ msgid "Player"
#~ msgstr "Reprodutor"

#~ msgid "APIs"
#~ msgstr "APIs"

#~ msgid ""
#~ "For a complete list of public APIs, see the <a href=\"https://github.com/"
#~ "TeamPiped/Piped/wiki/Instances\">Instances page</a>."
#~ msgstr ""
#~ "Para uma lista completa de APIs públicas, veja a <a href=\"https://github."
#~ "com/TeamPiped/Piped/wiki/Instances\">página de Instâncias</a> (em inglês)."

#~ msgid "Custom Piped API"
#~ msgstr "API Personalizada do Piped"

#~ msgid "Other"
#~ msgstr "Outros"

#~ msgid "Subscribe to a Channel…"
#~ msgstr "Inscreva-se em um canal…"

#~ msgid "Add Subscription"
#~ msgstr "Adicionar inscrição"

#~ msgid "Base URL"
#~ msgstr "URL base"

#~ msgid "Channel ID or Name"
#~ msgstr "ID ou nome do canal"

#~ msgctxt "accessibility"
#~ msgid "Add Subscription…"
#~ msgstr "Adicionar inscrição…"

#~ msgctxt "tooltip"
#~ msgid "Add Subscription…"
#~ msgstr "Adicionar inscrição…"

#~ msgctxt "accessibility"
#~ msgid "Go back to subscription list"
#~ msgstr "Voltar à lista de inscrições"

#~ msgctxt "tooltip"
#~ msgid "Go back"
#~ msgstr "Voltar"

#~ msgid "Please check your input, internet connection and API URL."
#~ msgstr ""
#~ "Por favor, confira os dados introduzidos, a sua conexão com a internet e "
#~ "a URL da API."

#~ msgid "Video Information"
#~ msgstr "Informações sobre o vídeo"

#~ msgid "Everything Watched"
#~ msgstr "Você já assistiu tudo"

#~ msgid "How about going outside?"
#~ msgstr "Que tal dar uma volta lá fora?"

#~ msgid "Error connecting to the network"
#~ msgstr "Erro ao se conectar à rede"

#~ msgid "Some error occured"
#~ msgstr "Um erro ocorreu"

#~ msgid "%F %T"
#~ msgstr "%F %T"

#~ msgid "Select NewPipe subscriptions file"
#~ msgstr "Selecionar arquivo de inscrições do NewPipe"

#~ msgid "Failure to import subscriptions"
#~ msgstr "Falha ao importar inscrições"

#~ msgid "Select YouTube subscription file"
#~ msgstr "Selecionar arquivo de inscrições do YouTube"

#~ msgid ""
#~ "Note that on Flatpak, there are some more steps required when using a "
#~ "player external to the Flatpak. For more information, please consult the "
#~ "wiki."
#~ msgstr ""
#~ "Note que, ao usar o Flatpak, alguns passos adicionais são necessários "
#~ "para usar um reprodutor externo ao Flatpak. Para mais informações, favor "
#~ "consultar a página wiki."

#~ msgid "Donate"
#~ msgstr "Faça uma doação"
